import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import NumberMain from './containers/number-main/NumberMain';
import Login from './containers/login/Login';
import Register from './containers/register/Register';
import Logout from './containers/logout/Logout';
import Spinner from './containers/spinner/Spinner';
import Play from './containers/play/Play';
import Authenticate from './containers/authenticate/Authenticate';
import Message from './containers/message/Message';
import History from './containers/history/History';
import Goal from './containers/goal/Goal';
import NavBar from './containers/navbar/NavBar';
import Footer from './containers/footer/Footer';
import { Helmet } from 'react-helmet';
import { GLOBAL_MESSAGE_TAG } from './contants/message-tag.const';
import { SITE_NAME, META_DESCRIPTION, META_TITLE, META_KEYWORD } from './contants/site.const';
import Forbidden from './containers/forbidden/Forbidden';
import Authorize from './containers/authorize/Authorize';
import { ADMINISTRATOR } from './contants/claim.const';
import ManageUser from './containers/manage-user/ManageUser';

function App() {
  return (
    <Provider store={store}>
      <Helmet defaultTitle={SITE_NAME} titleTemplate={`%s - ${SITE_NAME}`}>
        <meta name="title" content={META_TITLE} />
        <meta name="keywords" content={META_KEYWORD} />
        <meta name="description" content={META_DESCRIPTION} />
      </Helmet>
      <Spinner />
      <Router>
        <Message tag={GLOBAL_MESSAGE_TAG} />
        <NavBar></NavBar>
        <main className="my-5 py-4">
          <Switch>
            <Authenticate path="/play/:area?">
              <Play />
            </Authenticate>
            <Authenticate path="/goals">
              <Goal />
            </Authenticate>
            <Authenticate path="/history/:page?">
              <History />
            </Authenticate>
            <Authorize path="/admin/user" claims={[ADMINISTRATOR]}>
              <ManageUser />
            </Authorize>
            <Route path="/login" component={Login} />
            <Route path="/logout" component={Logout} />
            <Route path="/register" component={Register} />
            <Route path="/forbidden" component={Forbidden} />
            <Route path="/" component={NumberMain} exact />
          </Switch>
        </main>
        <Footer />
      </Router>
    </Provider>
  );
}

export default App;