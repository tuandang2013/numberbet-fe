import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, configure } from 'enzyme';
import { cleanup } from '@testing-library/react';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import AppNavbar from './components/app-navbar/AppNavbar';

configure({ adapter: new Adapter() });

describe('App', () => {
  describe('Initialization', () => {
    afterEach(cleanup);

    test('should renderer', () => {
      const div = document.createElement('div');

      ReactDOM.render(<App />, div);
    });

    test('should have navbar', () => {
      const app = shallow(<App />);

      expect(app.find(AppNavbar).length).toBe(1);
    });
  });
});