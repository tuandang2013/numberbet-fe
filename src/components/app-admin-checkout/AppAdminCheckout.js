import React from 'react';
import { useForm } from 'react-hook-form';
import AppFormError from '../app-form-error/AppFormError';

const AppAdminCheckout = ({ goalPoint, email, onSubmit, checkoutError }) => {
    const { register, handleSubmit, errors } = useForm({
        defaultValues: {
            amount: 0
        }
    });

    const formSubmit = ({ amount }) => onSubmit(amount);

    return (
        <form onSubmit={handleSubmit(formSubmit)}>
            <div className="table-responsive">
                <table className="table">
                    <tbody>
                        <tr>
                            <td>Email</td>
                            <td>{email}</td>
                        </tr>
                        <tr>
                            <td>Điểm</td>
                            <td>{goalPoint}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <AppFormError title="Thanh toán thất bại" error={checkoutError} />
            <div className="form-group">
                <label htmlFor="amount">Điểm thanh toán</label>
                <input ref={register({ required: true })}
                    name="amount"
                    type="number" className="form-control" id="amount"
                    aria-invalid={errors.amount ? "true" : "false"}
                    aria-describedby="amount-help" />
                <small id="amount-help" className="form-text text-danger" style={{ display: errors.amount ? "block" : "none" }} >
                    Điểm thanh toán phải lớn hơn 0
                </small>
            </div>
            <button type="submit" className="btn btn-primary">Thanh toán</button>
        </form>
    );
};

export default AppAdminCheckout;