import React from 'react';
import { NavDropdown } from 'react-bootstrap';
import { FaDatabase } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const AppAdminLinks = ({ pathname }) => (
    <NavDropdown className={pathname.startsWith('/admin') ? 'active' : ''} title={<span><FaDatabase /> Quản lý</span>} id="admin-nav-dropdown">
        <NavDropdown.Item className={pathname === '/admin/user' ? 'active' : ''} to="/admin/user" as={Link}>Tài khoản</NavDropdown.Item>
    </NavDropdown>
);

export default AppAdminLinks;