import React from 'react';
import { getPages } from '../../services/pager.service';

const AppButtonPager = ({ pageNumber, totalPages, onClick }) => {
    const limit = 5;
    const [pages, margin] = getPages(totalPages, pageNumber, limit);

    return (
        totalPages > 1 ?
            <div className="row">
                <div className="col">
                    <nav className="float-right">
                        <ul className="pagination">
                            {pageNumber > 1 && pageNumber - margin > 1 ?
                                <>
                                    <li className='page-item'>
                                        <button className='page-link' onClick={() => onClick(1)} aria-disabled="true">
                                            <span aria-hidden="true">&laquo;</span>
                                        </button>
                                    </li>
                                    <li className={`page-item ${pageNumber === 1 ? 'disabled' : ''}`}>
                                        <button className='page-link' onClick={() => onClick(pageNumber - 1)}>
                                            <span aria-hidden="true">...</span>
                                        </button>
                                    </li>
                                </> : null
                            }
                            {
                                pages.map(page => <li key={page} className={`page-item ${page === pageNumber ? 'active' : ''}`} aria-current="page">
                                    {page === pageNumber ?
                                        <span className="page-link">{page}</span> :
                                        <button className="page-link" onClick={() => onClick(page)}>{page}</button>
                                    }
                                </li>)
                            }
                            {pageNumber < totalPages && pageNumber + margin < totalPages ?
                                <>
                                    <li className={`page-item ${pageNumber === totalPages ? 'disabled' : ''}`}>
                                        <button className='page-link' onClick={() => onClick(pageNumber + 1)}>
                                            <span aria-hidden="true">...</span>
                                        </button>
                                    </li>
                                    <li className='page-item'>
                                        <button className='page-link' onClick={() => onClick(totalPages)}>
                                            <span aria-hidden="true">&raquo;</span>
                                        </button>
                                    </li>
                                </> : null
                            }
                        </ul>
                    </nav>
                </div>
            </div> : null
    );
}

export default AppButtonPager;