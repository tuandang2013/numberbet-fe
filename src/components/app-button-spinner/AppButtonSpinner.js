import React from 'react';
import { Button, Spinner } from 'react-bootstrap';

const AppButtonSpinner = ({ text = "Loading...", disabled = true } = {}) => {
    return (
        <Button variant="primary" disabled={disabled}>
            <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
            {text}
        </Button>
    );
};

export default AppButtonSpinner;