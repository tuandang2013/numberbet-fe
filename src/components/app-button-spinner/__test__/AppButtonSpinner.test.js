import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import AppButtonSpinner from '../AppButtonSpinner';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Button, Spinner } from 'react-bootstrap';

Enzyme.configure({ adapter: new Adapter() });

describe('AppButtonSpinner', () => {
    describe('Initialization', () => {
        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(<AppButtonSpinner />, div);
        });

        test('should default "Loading..." text', () => {
            const defaultText = "Loading...";
            const { getByText } = render(<AppButtonSpinner />);

            expect(getByText(defaultText)).toBeTruthy();
        });

        test('should default disabled button', () => {
            const wrapper = shallow(<AppButtonSpinner />);

            expect(
                wrapper.containsMatchingElement(
                    <Button variant="primary" disabled={true}>
                        <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
                        Loading...
                    </Button>)
            ).toBeTruthy();
        });

        test('should have text', () => {
            const text = 'Loading';
            const { getByText } = render(<AppButtonSpinner text={text} />);

            expect(getByText(text)).toBeTruthy();
        });

        test('should toggle disabled button', () => {
            const disabled = false;
            const wrapper = shallow(<AppButtonSpinner disabled={disabled} />);

            expect(
                wrapper.containsMatchingElement(
                    <Button variant="primary" disabled={disabled}>
                        <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
                        Loading...
                    </Button>)
            ).toBeTruthy();
        });

        test('should match snapshot', () => {
            const component = renderer.create(<AppButtonSpinner />);

            expect(component.toJSON()).toMatchSnapshot();
        });
    });
});