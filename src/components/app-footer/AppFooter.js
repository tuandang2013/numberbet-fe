import React from 'react';
import { SITE_NAME } from '../../contants/site.const';

const AppFooter = () => (
    <footer className="fixed-bottom bg-light">
        <hr className="mb-0"/>
        <div className="p-3">
            &copy;{SITE_NAME}
        </div>
    </footer>
);

export default AppFooter;