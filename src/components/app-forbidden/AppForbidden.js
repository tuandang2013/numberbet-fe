import React from 'react';

const AppForbidden = () => (
    <h1>Forbidden</h1>
);

export default AppForbidden;