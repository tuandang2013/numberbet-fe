import React from 'react';

const AppFormError = ({ error, title }) => {
    if (error == null)
        return null;
        
    const titleMessage = `${title ? title + ': ' : ''}`;
    const message = `${titleMessage}${error}`;

    return (
        typeof error === 'string' ? <div className="alert alert-danger">{message}</div> :
            <div className="alert alert-danger text-left">
                <strong>{titleMessage}</strong>
                <ul>
                    {error.map(e => <li key={e}>{e}</li>)}
                </ul>
            </div>
    );
};

export default AppFormError;