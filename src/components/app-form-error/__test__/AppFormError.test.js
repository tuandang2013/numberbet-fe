import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import AppFormError from '../AppFormError';
import renderer from 'react-test-renderer';

describe('AppFormError', () => {
    test('should rendered', () => {
        const div = document.createElement('div');

        ReactDOM.render(<AppFormError />, div);
    });

    test('should empty if error is null', () => {
        const { container } = render(<AppFormError />);

        expect(container).toBeEmpty();
    });

    test('should valid error message if error is string', () => {
        const error = 'error';
        const { getByText } = render(<AppFormError error={error} />);

        expect(getByText(error)).not.toBeNull();
    });

    test('should valid error message with title if presents', () => {
        const error = 'error';
        const title = 'title';
        const { getByText } = render(<AppFormError error={error} title={title} />);

        expect(getByText(`${title}: ${error}`)).not.toBeNull();
    });

    test('should match snapshot if error is string', () => {
        const error = 'error';
        const title = 'title';
        const component = renderer.create(<AppFormError error={error} title={title} />);

        expect(component.toJSON()).toMatchSnapshot();
    });

    test('should render list if error is not string', () => {
        const errors = ['error1', 'error2'];
        const title = 'title';
        const { getByText, container } = render(<AppFormError error={errors} title={title} />);
        const errorItems = container.querySelectorAll('li');

        expect(getByText(`${title}:`)).not.toBeNull();
        expect(errorItems.length).toBe(2);
    });

    test('should match snapshot if error is not string', () => {
        const errors = ['error1', 'error2'];
        const title = 'title';
        const component = renderer.create(<AppFormError error={errors} title={title} />);

        expect(component.toJSON()).toMatchSnapshot();
    });
});