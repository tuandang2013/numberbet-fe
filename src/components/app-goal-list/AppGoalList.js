import React from 'react';
import moment from 'moment';
import { goalReasons } from '../../services/goals.service';
import AppButtonPager from '../app-button-pager/AppButtonPager';

const AppGoalList = ({ data, pager, onChangePage }) => (
    <>
        <div className="table-responsive">
            <table className="table">
                <thead>
                    <tr>
                        <th>Ngày tạo</th>
                        <th>Thông tin</th>
                        <th className="text-right">Điểm</th>
                    </tr>
                </thead>
                <tbody>
                    {data.length === 0 ?
                        <tr>
                            <td colSpan="2">Hiện tại chưa có dữ liệu</td>
                        </tr> :
                        data.map((goal, index) => {
                            const record = goal.entity && goal.entity.target ? JSON.parse(goal.entity.target) : null;
                            return <tr key={index}>
                                <td>
                                    {moment(goal.createdDate).format('DD/MM/YYYY HH:mm')}
                                </td>
                                <td>
                                    {goalReasons[goal.reason]({ date: moment(goal.createdDate).format('DD/MM'), value: goal.value, record })}
                                </td>
                                <td className="text-right">
                                    <strong>
                                        {goal.value}
                                    </strong>
                                </td>
                            </tr>
                        })
                    }
                </tbody>
            </table>
        </div>
        <AppButtonPager {...pager} onClick={onChangePage} />
    </>
);

export default AppGoalList;