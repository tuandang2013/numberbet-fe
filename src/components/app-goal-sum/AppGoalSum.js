import React from 'react';
import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';
import { FaDollarSign, FaBtc } from 'react-icons/fa';

const AppGoalSum = ({ signedIn, info: { goalPoint, testGoalPoint } }) => {
    return (
        !signedIn ? null :
            <Nav.Link to="/goals" className='font-weight-bold' as={Link} data-testid='goals-link'>
                <FaDollarSign /> {goalPoint} | <FaBtc /> {testGoalPoint}
            </Nav.Link>
    );
};

export default AppGoalSum;