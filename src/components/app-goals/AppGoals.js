import React from 'react';
import { Container, Row, Col, Tabs, Tab } from 'react-bootstrap';
import AppGoalList from '../app-goal-list/AppGoalList';
import { GOAL_POINT, TEST_GOAL_POINT } from '../../contants/point.const';

const AppGoals = ({ goals, testGoals, currentGoal, currentTestGoal, onChangePageType }) => (
    <Container>
        <Row>
            <Col>
                <h1 className="display-2 mb-4">Điểm {currentGoal} | Điểm thưởng {currentTestGoal}</h1>

                <Tabs defaultActiveKey="goals">
                    <Tab eventKey="goals" title="Điểm">
                        <AppGoalList {...goals} onChangePage={onChangePageType(GOAL_POINT)} />
                    </Tab>
                    <Tab eventKey="test-goal" title="Điểm thưởng">
                        <AppGoalList {...testGoals} onChangePage={onChangePageType(TEST_GOAL_POINT)} />
                    </Tab>
                </Tabs>
            </Col>
        </Row>
    </Container>
);

export default AppGoals;