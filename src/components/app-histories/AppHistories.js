import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import AppHistoryList from '../app-history-list/AppHistoryList';

const AppHistories = ({ histories: { data, pager }, records, recordTypes, baseUrl }) => {
    return (
        <Container>
            <Row>
                <Col>
                    <h1 className="display-2 mb-4">Lược sử</h1>

                    <AppHistoryList histories={data} pager={pager} records={records} recordTypes={recordTypes} baseUrl={baseUrl} />
                </Col>
            </Row>
        </Container>
    );
};

export default AppHistories;