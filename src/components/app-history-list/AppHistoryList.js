import React from 'react';
import moment from 'moment';
import { FaCheck, FaTimes } from 'react-icons/fa';
import AppRecordSum from '../app-record-sum/AppRecordSum';
import AppButtonPager from '../app-button-pager/AppButtonPager';
import AppLinkPager from '../app-link-pager/AppLinkPager';

const AppHistoryList = ({ records, histories, pager, recordTypes, baseUrl, onClick, name }) => {
    return (
        <>
            <div className="table-responsive">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Ngày tạo</th>
                            <th>Thông tin</th>
                            <th className="text-center">
                                Kết quả
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {records.length === 0 ? null :
                            records.map((entry, index) => {
                                const type = recordTypes.find(type => type.info.code === entry.type);
                                return <tr key={index}>
                                    <td>
                                        {moment(entry.createdDate).format('DD/MM/YYYY HH:mm')}
                                    </td>
                                    <td>
                                        <AppRecordSum name={name} number={entry.number} point={entry.point} type={type} date={entry.date} pointType={entry.pointType} />
                                    </td>
                                    <td className="text-center align-middle">
                                    </td>
                                </tr>
                            })
                        }
                        {histories.length === 0 ? null :
                            histories.map((history, index) => {
                                const type = recordTypes.find(type => type.info.code === history.entry.type);
                                return <tr key={index}>
                                    <td>
                                        {moment(history.entry.createdDate).format('DD/MM/YYYY HH:mm')}
                                    </td>
                                    <td>
                                        <AppRecordSum name={name} number={history.entry.number} point={history.entry.point} type={type} date={history.entry.date} pointType={history.entry.pointType} />
                                    </td>
                                    <td className="text-center align-middle">
                                        {history.success ? <button className="btn btn-success btn-lg disabled"><FaCheck /></button> : <button className="btn btn-outline-info btn-lg disabled"><FaTimes /></button>}
                                    </td>
                                </tr>
                            })
                        }
                        {!histories.length && !records.length ?
                            <tr>
                                <td colSpan="3">Hiện tại chưa có dữ liệu</td>
                            </tr> : null
                        }
                    </tbody>
                </table>
            </div>
            {baseUrl ?
                <AppLinkPager {...pager} baseUrl={baseUrl} /> :
                <AppButtonPager {...pager} onClick={onClick} />}

        </>
    );
};

export default AppHistoryList;