import React from 'react';
import { Link } from 'react-router-dom';
import { getPages } from '../../services/pager.service';

const AppLinkPager = ({ pageNumber, totalPages, baseUrl }) => {
    const limit = 5;
    const [pages, margin] = getPages(totalPages, pageNumber, limit);

    return (
        totalPages > 0 ?
            <div className="row">
                <div className="col">
                    <nav className="float-right">
                        <ul className="pagination">
                            {pageNumber > 1 && pageNumber - margin > 1 ?
                                <>
                                    <li className='page-item'>
                                        <Link className="page-link" to={`${baseUrl}/1`}>
                                            <span aria-hidden="true">&laquo;</span>
                                        </Link>
                                    </li>
                                    <li className={`page-item ${pageNumber === 1 ? 'disabled' : ''}`}>
                                        <Link className="page-link" to={`${baseUrl}/${pageNumber - 1}`}>
                                            <span aria-hidden="true">...</span>
                                        </Link>
                                    </li>
                                </> : null
                            }
                            {
                                pages.map(page => {
                                    return <li key={page} className={`page-item ${page === pageNumber ? 'active' : ''}`} aria-current="page">
                                        {page === pageNumber ?
                                            <span className="page-link">{page}</span> :
                                            <Link className="page-link" to={`${baseUrl}/${page}`}>
                                                {page}
                                            </Link>
                                        }
                                    </li>
                                })
                            }
                            {pageNumber < totalPages && pageNumber + margin < totalPages ?
                                <>
                                    <li className={`page-item ${pageNumber === totalPages ? 'disabled' : ''}`}>
                                        <Link className="page-link" to={`${baseUrl}/${pageNumber + 1}`}>
                                            <span aria-hidden="true">...</span>
                                        </Link>
                                    </li>
                                    <li className='page-item'>
                                        <Link className="page-link" to={`${baseUrl}/${totalPages}`}>
                                            <span aria-hidden="true">&raquo;</span>
                                        </Link>
                                    </li>
                                </> : null
                            }
                        </ul>
                    </nav>
                </div>
            </div> : null
    );
}

export default AppLinkPager;