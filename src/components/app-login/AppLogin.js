import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Spinner } from 'react-bootstrap';
import AppFormError from '../app-form-error/AppFormError';
import './AppLogin.css';

const AppLogin = ({ onSubmit, error, processing }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [remember, setRemember] = useState(false);

    function handleSubmit(e) {
        e.preventDefault();

        if (!processing)
            onSubmit(email, password, remember);
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col>
                    <div className="text-center">
                        <form className="form-signin" onSubmit={handleSubmit}>
                            <h1 className="mb-3 font-weight-normal">Đăng nhập</h1>
                            <AppFormError title="Đăng nhập thất bại" error={error} />
                            <label htmlFor="inputEmail" className="sr-only">Email</label>
                            <input name="email" value={email} onChange={e => setEmail(e.target.value)} type="email" id="inputEmail" className="form-control" placeholder="Email" required autoFocus />
                            <label htmlFor="inputPassword" className="sr-only">Mật khẩu</label>
                            <input name="password" value={password} onChange={e => setPassword(e.target.value)} type="password" id="inputPassword" className="form-control" placeholder="Mật khẩu" required/>
                            <div className="checkbox mb-3">
                                <label>
                                    <input type="checkbox" value={remember} onChange={e => setRemember(e.target.checked)} name="remember-me" /> Ghi nhớ
                                </label>
                            </div>
                            <button className={`btn btn-lg btn-primary btn-block ${processing ? 'disabled' : ''}`} type="submit">
                                {processing ? <Spinner as="span" className="mb-1" animation="grow" size="sm" role="status" aria-hidden="true" /> : null}
                                Đăng nhập
                            </button>
                        </form>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default AppLogin;