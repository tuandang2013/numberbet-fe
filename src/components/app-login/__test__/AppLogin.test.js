import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import AppLogin from '../AppLogin';

Enzyme.configure({ adapter: new Adapter() });

describe('AppLogin', () => {
    describe('Initialization', () => {
        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(<AppLogin />, div);
        });

        test('should match snapshot', () => {
            const loginComp = renderer.create(<AppLogin />);

            expect(loginComp).toMatchSnapshot();
        });

        test('should have title', () => {
            const wrapper = shallow(<AppLogin />);
            const h1 = wrapper.find('h1');

            expect(h1.text()).toEqual('Sign in');
        });

        test('should have email input', () => {
            const wrapper = shallow(<AppLogin />);
            const email = wrapper.exists('input[name="email"]');

            expect(email).toBeTruthy();
        });

        test('should have password input', () => {
            const wrapper = shallow(<AppLogin />);
            const password = wrapper.exists('input[name="password"][type="password"]');

            expect(password).toBeTruthy();
        });

        test('should have remember me checkbox', () => {
            const wrapper = shallow(<AppLogin />);
            const rememberMe = wrapper.exists('input[name="remember-me"][type="checkbox"]');

            expect(rememberMe).toBeTruthy();
        });

        test('should have submit button', () => {
            const wrapper = shallow(<AppLogin/>);
            const submitBtn = wrapper.find('input[type="submit"]');

            expect(submitBtn).toBeTruthy();
        });
    });
});