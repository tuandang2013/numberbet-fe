import React from 'react';
import moment from 'moment';
import { Toast } from 'react-bootstrap';

const AppMessage = ({ title, message, error, createdOn, remove }) => (
    <div className="mb-3">
        <div>
            <Toast show={true} onClose={remove} className={error ? 'bg-danger text-white' : ''}>
                <Toast.Header className={error ? 'bg-danger text-white' : ''}>
                    {title ? <strong className="mr-auto">{title || 'Thông báo'}</strong> : ''}
                    <small>{moment(createdOn).fromNow()}</small>
                </Toast.Header>
                <Toast.Body>
                    <div dangerouslySetInnerHTML={{ __html: message }}></div>
                </Toast.Body>
            </Toast>
        </div>
    </div>
);

export default AppMessage;