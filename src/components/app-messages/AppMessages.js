import React from 'react';
import AppMessage from '../app-message/AppMessage';

const AppMessages = ({ messages = [], onRemove }) => {
    return (
        <div className="position-absolute mr-sm-3 mt-4" style={{ right: 0, zIndex: 1000 }}>
            {messages.map((item, index) =>
                <AppMessage {...item} key={index} remove={() => onRemove(item)} />
            )}
        </div>
    );
};

export default AppMessages;