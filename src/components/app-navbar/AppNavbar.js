import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AppUserInfo from '../app-user-info/AppUserInfo';
import AppPlayButton from '../app-play-button/AppPlayButton';
import { FaBolt, FaHome } from 'react-icons/fa';
import { SITE_NAME } from '../../contants/site.const';
import AppGoalSum from '../app-goal-sum/AppGoalSum';
import AppAdminLinks from '../app-admin-links/AppAdminLinks';

const AppNavbar = ({ pathname, user, isAdmin }) => {
    return (
        <Navbar bg="light" fixed="top" expand="md">
            <Navbar.Brand to="/" as={Link}>{SITE_NAME}</Navbar.Brand>
            <Nav className="ml-auto mr-2 mr-md-3 d-block d-md-none">
                <AppGoalSum {...user} />
            </Nav>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link to="/" className={pathname === '/' ? 'active' : ''} as={Link} data-testid='home-link'>
                        <FaHome /><span className="d-inline d-md-none d-lg-inline">&nbsp;Trang chủ</span>
                    </Nav.Link>
                    <Nav.Link to="/play" className={pathname.startsWith('/play') ? 'active' : ''} as={Link} data-testid='play-link'><FaBolt /> Chơi ngay</Nav.Link>
                    {isAdmin ? <AppAdminLinks pathname={pathname} /> : null}
                </Nav>
                <AppUserInfo {...user} pathname={pathname} />
                <AppPlayButton />
            </Navbar.Collapse>
        </Navbar>
    );
};

export default AppNavbar;