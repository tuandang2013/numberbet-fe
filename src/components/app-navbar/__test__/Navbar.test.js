import React from 'react';
import ReactDOM from 'react-dom';
import AppNavbar from '../AppNavbar';
import { StaticRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import { cleanup, render } from '@testing-library/react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AppUserInfo from '../../app-user-info/AppUserInfo';
import AppPlayButton from '../../app-play-button/AppPlayButton';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

Enzyme.configure({ adapter: new Adapter() });

jest.mock('../../app-user-info/AppUserInfo', () => 'app-user-info');

describe('Navbar', () => {
    describe('Initialization', () => {
        afterEach(cleanup);

        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(
                <StaticRouter>
                    <AppNavbar />
                </StaticRouter>, div);
        });

        test('should match snapshot', () => {
            const appNavbar = renderer.create(
                <StaticRouter>
                    <AppNavbar />
                </StaticRouter>);

            expect(appNavbar).toMatchSnapshot();
        });

        test('should have valid site name', () => {
            const siteName = 'Lode.com';
            const { getByText } = render(
                <StaticRouter><AppNavbar /></StaticRouter>
            );

            const siteNameElement = getByText(siteName);
            expect(siteNameElement).toBeTruthy();
        });

        test('should contains user info', () => {
            const wrapper = shallow(<AppNavbar />);

            expect(wrapper.find(AppUserInfo).length).toEqual(1);
        });

        test('should have play button', () => {
            const wrapper = shallow(<AppNavbar />);

            expect(wrapper.find(AppPlayButton).length).toEqual(1);
        });

        it('should have home link', () => {
            const wrapper = shallow(<AppNavbar />);
            const homeLink = wrapper.find(Nav.Link)
                .find({ to: '/' }).first();

            expect(homeLink).toBeTruthy();
            expect(homeLink.text()).toEqual('Home');
        });

        it('should have play link', () => {
            const wrapper = shallow(<AppNavbar />);
            const playLink = wrapper.find(Nav.Link)
                .find({ to: '/play' }).first();

            expect(playLink).toBeTruthy();
            expect(playLink.text()).toEqual('Play');
        });
    });
});