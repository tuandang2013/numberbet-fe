import React from 'react';
import moment from 'moment';
import goalNames from '../../services/goalNames';

const AppNumberTable = ({ name, date, day, channels }) => {
    if (!(name && channels && channels.length)) return <></>;

    const mDate = moment(date);
    const mDateFormat = mDate.isValid() ? mDate.format('DD/MM') : '';
    const channelNames = channels.map(ch => ch.name);
    const channelGoals = channels.map(ch => (ch.goals || []).reduce((channelGoal, goal) => {
        channelGoal[goal.code] = goal.numbers;
        return channelGoal;
    }, {}));
    const colSize = Math.ceil(12 / channels.length);

    return (
        <div data-testid="table-numbers">
            <h2>{name}</h2>
            <ul className="list-group list-group-flush">
                <li className="list-group-item px-0">
                    <div className="row">
                        <div className="col-2 text-center">
                            <strong>{day} {mDateFormat}</strong>
                        </div>
                        <div className="col-10 text-center">
                            <div className="row">
                                {channelNames.map(name => <div className="col" key={name}>
                                    <strong>{name}</strong>
                                </div>)}
                            </div>
                        </div>
                    </div>
                </li>
                {goalNames.map((goal, index) =>
                    channelGoals.some(chGoals => chGoals[goal]) &&
                    <li className="list-group-item px-0 text-center" key={goal}>
                        <div className="row">
                            <div className="col-2">
                                {index === goalNames.length - 1 ? <strong>{goal}</strong> : goal}
                            </div>
                            <div className="col-10">
                                <div className="row">
                                    {channelGoals.map((chGoals, chIndex) => {
                                        return (
                                            <div className={`col-${colSize} text-wrap`} key={chIndex}>
                                                {index === goalNames.length - 1 ?
                                                    <strong>{chGoals[goal].join(' - ')}</strong> :
                                                    <span>{chGoals[goal].join(' - ')}</span>}
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                    </li>
                )}
            </ul>
        </div>
    );
};

export default AppNumberTable;