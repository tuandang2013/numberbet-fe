import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import AppNumberTable from '../AppNumberTable';
import goalNames from '../../../services/goalNames';

describe('AppNumberTable', () => {
    describe('Initialization', () => {
        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(<AppNumberTable />, div);
        });

        test('should not render if don\'t have prop name', () => {
            const name = null;
            const { container } = render(<AppNumberTable name={name} />);

            expect(container.firstChild).toBeFalsy();
        });

        test('should not render if channels empty', () => {
            const name = 'name';
            const channels = [];
            const { container } = render(<AppNumberTable name={name} channels={channels} />);

            expect(container.firstChild).toBeFalsy();
        });

        test('should have title', () => {
            const name = 'name';
            const channels = [{name: 'channel'}];
            const { getByText } = render(<AppNumberTable name={name} channels={channels} />);

            const titleElement = getByText(name);
            expect(titleElement).toBeTruthy();
        });

        test('should have table', () => {
            const name = 'name';
            const channels = [{name: 'channel'}];
            const { getByTestId } = render(<AppNumberTable name={name} channels={channels} />);

            const tableElement = getByTestId('table-numbers');
            expect(tableElement).toBeTruthy();
        });

    });
});