import React from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import AppNumberTable from '../app-number-table/AppNumberTable';
import moment from 'moment';

const AppNumber = ({ date, numbers: { north, south } = {} }) => {
    const mDate = moment(date);
    const mDateFormat = mDate.isValid() ? mDate.format('DD/MM/YYYY') : '';
    const mDateShortFormat = mDate.isValid() ? mDate.format('DD/MM') : '';

    return (!north || !south ? null : <Container>
        <Row>
            <Col>
                <h1 className="display-2 mb-4">
                    Kết quả xổ số
                    <small className="text-muted d-inline d-lg-none">&nbsp;{mDateShortFormat}</small>
                    <small className="text-muted d-none d-lg-inline">&nbsp;{mDateFormat}</small>
                </h1>
            </Col>
        </Row>
        <Row>
            <Col>
                <AppNumberTable {...north} />
            </Col>
        </Row>
        <Row>
            <Col>
                <AppNumberTable {...south} />
            </Col>
        </Row>
    </Container>
    );
};

export default AppNumber;