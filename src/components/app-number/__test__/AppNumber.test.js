import React from 'react';
import ReactDOM from 'react-dom';
import AppNumber from '../AppNumber';
import { render } from '@testing-library/react';

describe('AppNumber', () => {
    describe('Initialization', () => {
        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(<AppNumber />, div);
        });

        test('should have title', () => {
            const numbers = {north: {}, south: {}};
            const { getByText } = render(<AppNumber numbers={numbers}/>);

            const titleElement = getByText('Kết quả xổ số');
            expect(titleElement).toBeTruthy();
        });

        test('should have date', () => {
            const numbers = {north: {}, south: {}};
            const date = '2020-02-16';
            const { getByText } = render(<AppNumber date={date} numbers={numbers}/>);

            const dateElement = getByText('16/02/2020');
            expect(dateElement).toBeTruthy();
        });
    });
});