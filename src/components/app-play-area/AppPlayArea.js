import React, { useState, useEffect, useRef } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FaCheck, FaTimes, FaArrowCircleRight } from 'react-icons/fa';
import moment from 'moment';
import AppFormError from '../app-form-error/AppFormError';
import AppPlayConfirm from '../app-play-confirm/AppPlayConfirm';
import { GOAL_POINT, TEST_GOAL_POINT } from '../../contants/point.const';

const AppPlayArea = ({ area: areaName, pointInfo, recordInfo, recordError, onSubmit, onCanPlay, onCannotPlay }) => {
    const { goalPoint, testGoalPoint } = pointInfo;
    const area = recordInfo.areas.find(area => area.code === areaName);
    const recordDate = moment(recordInfo.date);

    //try get can play type
    let initialType;
    for (const type of recordInfo.types) {
        if (goalPoint >= type.pointRate[areaName] || testGoalPoint >= type.pointRate[areaName]) {
            initialType = type;
            break;
        }
    }
    if (initialType == null) {
        initialType = recordInfo.types[0];
    }

    const [type, setType] = useState(initialType);
    const [channel, setChannel] = useState(area.channels[0]);
    const [point, setPoint] = useState(1);
    const [number, setNumber] = useState('');
    const [pointType, setPointType] = useState(GOAL_POINT);
    const [pointTypeReadOnly, setPointTypeReadOnly] = useState(false);

    const [numberError, setNumberError] = useState('');
    const [pointError, setPointError] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const [canPlay, setCanPlay] = useState(goalPoint < type.pointRate[areaName] && testGoalPoint < type.pointRate[areaName]);

    const [showConfirm, setShowConfirm] = useState(false);

    let numberRegex = useRef(new RegExp(type.pattern));

    useEffect(() => {
        if (goalPoint < type.pointRate[areaName] && testGoalPoint < type.pointRate[areaName]) {
            setCanPlay(false);
            onCannotPlay(type.info.name);
        } else {
            setCanPlay(true);
            onCanPlay();
        }
    }, [type, areaName, goalPoint, testGoalPoint, onCanPlay, onCannotPlay]);

    useEffect(() => {
        setChannel(area.channels[0]);
    }, [area]);

    useEffect(() => {
        setNumber('');
        
        numberRegex.current = new RegExp(type.pattern);
    }, [type]);

    useEffect(() => {
        if (canPlay) {
            if (goalPoint < type.pointRate[areaName]) {
                if (testGoalPoint >= type.pointRate[areaName]) {
                    setPointType(TEST_GOAL_POINT);
                    setPointTypeReadOnly(true);
                }
            } else if (testGoalPoint < type.pointRate[areaName]) {
                if (goalPoint >= type.pointRate[areaName]) {
                    setPointType(GOAL_POINT);
                    setPointTypeReadOnly(true);
                }
            } else {
                setPointTypeReadOnly(false);
            }
        } else {
            setPointTypeReadOnly(true);
        }
    }, [type, areaName, goalPoint, testGoalPoint, canPlay]);

    useEffect(() => {
        if (submitted) {
            isValidNumber(number);
        }
    });

    useEffect(() => {
        if (submitted) {
            isValidPoint(point, pointType === TEST_GOAL_POINT);
        }
    });

    function isValidNumber(number) {
        if (number.length === 0) {
            setNumberError('Please enter number');
            return false;
        }
        if (!numberRegex.current.test(number)) {
            setNumberError('Number is invalid');
            return false;
        }

        setNumberError('');
        return true;
    }

    function isValidPoint(point, isTest) {
        if (point <= 0) {
            setPointError('Point must be greater than zero');
            return false;
        }

        if (isTest) {
            if (testGoalPoint < point * type.pointRate[areaName]) {
                setPointError('You don\'t have enough test points');
                return false;
            }
        } else {
            if (goalPoint < point * type.pointRate[areaName]) {
                setPointError('You don\'t have enough points');
                return false;
            }
        }
        setPointError('');
        return true;
    }

    function confirm() {
        setSubmitted(true);
        const validNumber = isValidNumber(number);
        const validPoint = isValidPoint(point, pointType === TEST_GOAL_POINT);

        if (!validNumber || !validPoint) {
            return;
        }

        setShowConfirm(true);
    }

    function hideConfirm() {
        setShowConfirm(false);
    }

    function submit() {
        setSubmitted(true);
        const validNumber = isValidNumber(number);
        const validPoint = isValidPoint(point, pointType === TEST_GOAL_POINT);

        if (!validNumber || !validPoint) {
            return;
        }

        setShowConfirm(false);

        onSubmit({
            area: areaName,
            channel,
            type: type.info.code,
            point: point * type.pointRate[areaName],
            pointType,
            number,
            date: recordDate.format('YYYY-MM-DD')
        });
    }

    return (
        <Container>
            <Row>
                <Col>
                    <h1 className="display-2">Đặt số</h1>
                    <div className="my-4">
                        <ul className="list-group list-group-horizontal">
                            {recordInfo.areas.map(ar =>
                                <Link key={ar.code} className={`list-group-item list-group-item-primary list-group-item-action flex-fill text-center ${ar.code === areaName ? "active" : ""}`} to={`/play/${ar.code}`}>
                                    {ar.name} {ar.code === areaName ? <FaCheck /> : null}
                                </Link>
                            )}
                        </ul>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-4 col-lg-3 mb-3">
                            <div className="list-group">
                                <button type="button" disabled className="list-group-item list-group-item-action active">
                                    Tổng đài
                                </button>
                                {area.channels.map(ch =>
                                    <button key={ch} type="button" className={`list-group-item list-group-item-action ${!canPlay ? 'disabled' : ''} ${ch === channel ? "list-group-item-info" : ""}`}
                                        onClick={() => setChannel(ch)}>
                                        {ch} {ch === channel ? <FaCheck /> : null}
                                    </button>
                                )}
                            </div>
                        </div>
                        <div className="col-12 col-md-8 col-lg-9">
                            <div className="row">
                                {canPlay ?
                                    <div className="col-12">
                                        <AppFormError title="Error" error={recordError} />
                                    </div> :
                                    null
                                }
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="number">Chọn số</label>
                                        <div>
                                            <input type="text" disabled={!canPlay} value={number} maxLength={type.numberCount} className="form-control form-control-lg" placeholder="Chọn số" id="number"
                                                onChange={e => setNumber(e.target.value)} />
                                        </div>
                                        {canPlay && numberError && <small className="form-text text-danger">{numberError}</small>}
                                    </div>
                                    <div className="form-group">
                                        <div>
                                            {recordInfo.types.map(tp =>
                                                <button key={tp.info.code} className={`btn ${tp.info.code === type.info.code ? "btn-light" : "btn-light"} btn-lg mr-3`} type="button"
                                                    onClick={() => setType(tp)}>
                                                    {tp.info.name} {tp.info.code === type.info.code ? (canPlay ? <FaCheck /> : <FaTimes />) : null}
                                                </button>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6 mb-2">
                                    <div className="form-group">
                                        <label htmlFor="point">Điểm chơi</label>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">{type.pointRate[areaName]} x </span>
                                            </div>
                                            <input type="number" disabled={!canPlay} value={point} className="form-control form-control-lg" placeholder="Điểm chơi" id="point"
                                                min="1"
                                                onChange={e => setPoint(e.target.value)} />
                                            <div className="input-group-append">
                                                <span className="input-group-text"> =&nbsp;<strong>{type.pointRate[areaName] * point}</strong></span>
                                            </div>
                                        </div>
                                        {canPlay && pointError && <small className="form-text text-danger">{pointError}</small>}
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" disabled={!canPlay || pointTypeReadOnly} type="checkbox" checked={pointType === TEST_GOAL_POINT} id="testPoint"
                                            onChange={e => setPointType(e.target.checked ? TEST_GOAL_POINT : GOAL_POINT)} />
                                        <label className="form-check-label" htmlFor="testPoint">
                                            Sử dụng điểm thưởng
                                                </label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="form-group">
                                        <label htmlFor="date">Ngày chơi</label>
                                        <div>
                                            <input type="text" readOnly value={recordDate.format('DD/MM/YYYY')} className="form-control form-control-lg" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <button type="button" disabled={!canPlay} className="btn btn-primary btn-lg" onClick={confirm}>Xác nhận <FaArrowCircleRight /></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <AppPlayConfirm area={areaName} channel={channel} number={number} type={type} point={point} pointType={pointType} date={recordDate} show={showConfirm} onHide={hideConfirm} onConfirm={submit} />
                </Col>
            </Row>
        </Container>
    );
};

export default AppPlayArea;