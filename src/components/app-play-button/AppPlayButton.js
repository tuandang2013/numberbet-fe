import React from 'react';
import { Link } from 'react-router-dom';
import { FaBolt } from 'react-icons/fa';

const AppPlayButton = () => {

    return (
        <Link className="btn btn-success ml-3 d-lg-inline-block d-none" to="/play"><FaBolt /> Chơi ngay</Link>
    );
};

export default AppPlayButton;