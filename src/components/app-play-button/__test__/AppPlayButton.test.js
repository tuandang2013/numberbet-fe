import React from 'react';
import ReactDOM from 'react-dom';
import AppPlayButton from '../AppPlayButton';
import { StaticRouter as Router } from 'react-router-dom';

describe('AppPlayButton', () => {
    test('should rendered', () => {
        const div = document.createElement('div');

        ReactDOM.render(<Router>
            <AppPlayButton />
        </Router>, div);
    });
});