import React from 'react';
import { Modal, Button, Table } from 'react-bootstrap';

const AppPlayConfirm = ({ show, area, channel, number, type, point, pointType, date, onHide, onConfirm }) => (
    show ? <Modal show={show}>
        <Modal.Header closeButton>
            <Modal.Title>Xác nhận</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            Vui lòng xác nhận những thông tin sau đây:
            <Table className="mt-3">
                <tbody>
                    <tr>
                        <td>Ngày chơi</td>
                        <td>
                            <strong>
                                {date.format('DD/MM/YYYY')}
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Khu vực</td>
                        <td>
                            <strong>
                                {area === 'north' ? 'Miền Bắc' : 'Miền Nam'}
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Tổng đài</td>
                        <td>
                            <strong>{channel}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Con số</td>
                        <td>
                            <strong>
                                {number}
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Cách chơi</td>
                        <td>
                            <strong>
                                {type.info.name}
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Điểm chơi</td>
                        <td>
                            <strong>{point * type.pointRate[area]} {pointType === 'test-goal-point' ? '(điểm thưởng)' : ''}</strong>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="light" onClick={onHide}>
                Đóng</Button>
            <Button variant="primary" onClick={onConfirm}>
                Gửi đi</Button>
        </Modal.Footer>
    </Modal> : null
);

export default AppPlayConfirm;