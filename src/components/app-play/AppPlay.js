import React from 'react';
import { Container, Row, Col, ListGroup } from 'react-bootstrap';
import { Link, useRouteMatch } from 'react-router-dom';
import { NORTH_AREA, SOUTH_AREA } from '../../contants/area.const';

const AppPlay = () => {
    const { url } = useRouteMatch();

    return (
        <Container>
            <Row>
                <Col>
                    <ListGroup className="text-center mt-2">
                        <ListGroup.Item to={`${url}/${NORTH_AREA}`} as={Link} action variant="primary" className="py-5" data-testid="north-link">
                            <h2 className="display-4">Miền Bắc</h2>
                        </ListGroup.Item>
                        <ListGroup.Item to={`${url}/${SOUTH_AREA}`} as={Link} action variant="danger" className="py-5" data-testid="south-link">
                            <h2 className="display-4">Miền Nam</h2>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    );
};

export default AppPlay;