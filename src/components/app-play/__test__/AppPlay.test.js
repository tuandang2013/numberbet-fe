import React from 'react';
import ReactDOM from 'react-dom';
import AppPlay from '../AppPlay';
import { MemoryRouter } from 'react-router-dom';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { render } from '@testing-library/react';

configure({ adapter: new Adapter() });

describe('AppPlay', () => {
    it('should rendered', () => {
        const div = document.createElement('div');

        ReactDOM.render(
            <MemoryRouter initialEntries={['/play']}>
                <AppPlay></AppPlay>
            </MemoryRouter>, div
        );
    });

    it('should have north link and south link', () => {
        const { getByTestId } = render(
            <MemoryRouter initialEntries={['/play']}>
                <AppPlay />
            </MemoryRouter>
        );

        expect(getByTestId('north-link')).toBeTruthy();
        expect(getByTestId('south-link')).toBeTruthy();
    });
});