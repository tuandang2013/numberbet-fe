import React from 'react';
import moment from 'moment';
import { TEST_GOAL_POINT } from '../../contants/point.const';

const AppRecordSum = ({ number, point, pointType, date, name = 'Bạn' }) => (
    <p>{name} đã đặt
            số <strong>{number} </strong>
            với <strong>{point} điểm {pointType === TEST_GOAL_POINT ? 'thưởng' : ''} </strong>
            ngày <strong>{moment(date).format('DD/MM')}</strong>.
    </p>
);

export default AppRecordSum;