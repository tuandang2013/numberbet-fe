import React from 'react';
import moment from 'moment';
import { TEST_GOAL_POINT } from '../../contants/point.const';

const AppRecordTable = ({ number, type, point, pointType, date }) => (
    <table className="table table-sm">
        <tbody>
            <tr>
                <td className="border-0">Số</td>
                <td className="border-0">
                    {number}
                </td>
            </tr>
            <tr>
                <td>Loại</td>
                <td>
                    {type ? type.info.name : ''}
                </td>
            </tr>
            <tr>
                <td>Điểm</td>
                <td>
                    {point} {pointType === TEST_GOAL_POINT ? '(Điểm thưởng)' : ''}
                </td>
            </tr>
            <tr>
                <td>Ngày</td>
                <td>
                    {moment(date).format('DD/MM/YYYY')}
                </td>
            </tr>
        </tbody>
    </table>
);

export default AppRecordTable;