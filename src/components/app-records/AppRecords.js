import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import moment from 'moment';
import { FaCheck, FaTimes } from 'react-icons/fa';
import AppRecordSum from '../app-record-sum/AppRecordSum';

const AppRecords = ({ records, recordTypes }) => {
    return (
        <Container>
            <Row className="mt-5">
                <Col className="mt-5">
                    <h1 className="display-2 mb-4">Lược sử</h1>

                    <div className="table-responsive">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Ngày tạo</th>
                                    <th>Thông tin</th>
                                    <th className="text-center">
                                        Kết quả
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {records.length === 0 ?
                                    <tr>
                                        <td colSpan="3">Hiện tại chưa có dữ liệu</td>
                                    </tr> :
                                    records.map((entry, index) => {
                                        const type = recordTypes.find(type => type.info.code === entry.type);
                                        return <tr key={index}>
                                            <td>
                                                {moment(entry.createdDate).format('DD/MM/YYYY HH:mm')}
                                            </td>
                                            <td>
                                                <AppRecordSum number={entry.number} point={entry.point} type={type} date={entry.date} pointType={entry.pointType} />
                                            </td>
                                            <td className="text-center align-middle">
                                                {entry.success ? <button className="btn btn-success btn-lg disabled"><FaCheck /></button> : <button className="btn btn-outline-info btn-lg disabled"><FaTimes /></button>}
                                            </td>
                                        </tr>
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </Col>
            </Row>
        </Container >
    );
};

export default AppRecords;