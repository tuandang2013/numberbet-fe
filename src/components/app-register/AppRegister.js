import React, { useState } from 'react';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import AppFormError from '../app-form-error/AppFormError';
import './AppRegister.css';

const AppRegister = ({ onSubmit, error, processing }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [phone, setPhone] = useState('');

    function handleSubmit(e) {
        e.preventDefault();
        
        if (!processing)
            onSubmit(email, password, confirmPassword, phone);
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col>
                    <div className="text-center">
                        <form className="form-register" onSubmit={handleSubmit}>
                            <h1 className="mb-3 font-weight-normal">Đăng ký</h1>
                            <AppFormError title="Đăng ký thất bại" error={error} />
                            <label htmlFor="inputEmail" className="sr-only">Email</label>
                            <input name="email" value={email} onChange={e => setEmail(e.target.value)} type="email" id="inputEmail" className="form-control" placeholder="Email" required autoFocus />
                            <label htmlFor="inputPassword" className="sr-only">Mật khẩu</label>
                            <input name="password" value={password} onChange={e => setPassword(e.target.value)} type="password" id="inputPassword" className="form-control" placeholder="Mật khẩu" required minLength="6"/>
                            <label htmlFor="confirmPassword" className="sr-only">Xác nhận mật khẩu</label>
                            <input name="confirmPassword" value={confirmPassword} onChange={e => setConfirmPassword(e.target.value)} type="password" id="confirmPassword" className="form-control" placeholder="Xác nhận mật khẩu" required />
                            <label htmlFor="phone" className="sr-only">Điện thoại</label>
                            <input name="phone" value={phone} onChange={e => setPhone(e.target.value)} type="text" id="phone" className="form-control end" placeholder="Điện thoại" required />
                            <button className={`btn btn-lg btn-primary btn-block ${processing ? 'disabled' : ''}`} type="submit">
                                {processing ? <Spinner as="span" className="mb-1" animation="grow" size="sm" role="status" aria-hidden="true" /> : null}
                                Đăng ký
                            </button>
                        </form>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default AppRegister;