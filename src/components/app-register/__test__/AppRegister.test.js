import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import AppRegister from '../AppRegister';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('AppRegister', () => {
    describe('Initialization', () => {
        test('should renderred', () => {
            const div = document.createElement('div');

            ReactDOM.render(<AppRegister />, div);
        });

        test('should have title', () => {
            const title = 'Register';
            const { getByText } = render(<AppRegister />);

            expect(getByText(title)).toBeTruthy();
        });

        test('should have email input', () => {
            const wrapper = shallow(<AppRegister />);
            const emailInput = wrapper.exists('input[name="email"]');

            expect(emailInput).toBeTruthy();
        });

        test('should have password input', () => {
            const wrapper = shallow(<AppRegister />);
            const passwordInput = wrapper.exists('input[name="password"]');

            expect(passwordInput).toBeTruthy();
        });

        test('should have confirm password input', () => {
            const wrapper = shallow(<AppRegister />);
            const confirmPassword = wrapper.exists('input[name="confirmPassword"]');

            expect(confirmPassword).toBeTruthy();
        });

        test('should have phone input', () => {
            const wrapper = shallow(<AppRegister />);
            const phoneInput = wrapper.exists('input[name="phone"]');

            expect(phoneInput).toBeTruthy();
        });

        test('should have submit button', () => {
            const wrapper = shallow(<AppRegister />);
            const submitBtn = wrapper.find('input[type="submit"]');

            expect(submitBtn).toBeTruthy();
        });

        test('should match snapshot', () => {
            const component = renderer.create(<AppRegister />);

            expect(component.toJSON()).toMatchSnapshot();
        });
    });
});