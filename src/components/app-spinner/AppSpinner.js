import React from 'react';
import { Spinner } from 'react-bootstrap';
import './AppSpinner.css';

const AppSpinner = () => {
    return (
        <div className="app-spinner">
            <Spinner animation="grow" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        </div>
    );
};

export default AppSpinner;