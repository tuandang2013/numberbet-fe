import React from 'react';
import ReactDOM from 'react-dom';
import AppSpinner from '../AppSpinner';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Spinner } from 'react-bootstrap';
import renderer from 'react-test-renderer';

Enzyme.configure({ adapter: new Adapter() });

describe('AppSpinner', () => {
    describe('Initialization', () => {
        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(<AppSpinner />, div);
        });

        test('should contains spinner if processing', () => {
            const wrapper = shallow(<AppSpinner />);

            expect(wrapper.containsMatchingElement(
                <Spinner animation="grow" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>)).toBeTruthy();
        });

        test('should match snapshot', () => {
            const component = renderer.create(<AppSpinner />);

            expect(component.toJSON()).toMatchSnapshot();
        });
    });
});