import React from 'react';
import { useForm } from 'react-hook-form';
import AppFormError from '../app-form-error/AppFormError';

const AppUserChangePassword = ({ onSubmit, changePassError }) => {
    const { register, handleSubmit, errors } = useForm();

    const formSubmit = data => onSubmit(data);

    return (
        <form onSubmit={handleSubmit(formSubmit)}>
            <AppFormError title="Đổi mật khẩu thất bại" error={changePassError} />
            <div className="form-group">
                <label htmlFor="new-password">Mật khẩu</label>
                <input ref={register({ required: true })}
                    name="password"
                    type="password" className="form-control" id="new-password"
                    aria-invalid={errors.password ? "true" : "false"}
                    aria-describedby="new-password-help" />
                <small id="new-password-help" className="form-text text-danger" style={{ display: errors.password ? "block" : "none" }} >
                    Vui lòng nhập mật khẩu
                </small>
            </div>
            <div className="form-group">
                <label htmlFor="confirm-password">Xác nhận mật khẩu</label>
                <input ref={register({ required: true })}
                    name="confirmPassword"
                    type="password" className="form-control" id="confirm-password"
                    aria-invalid={errors.confirmPassword ? "true" : "false"}
                    aria-describedby="confirm-password-help" />
                <small id="confirm-password-help" className="form-text text-danger" style={{ display: errors.confirmPassword ? "block" : "none" }} >
                    Vui lòng xác nhận mật khẩu
                </small>
            </div>
            <button type="submit" className="btn btn-primary">Đổi mật khẩu</button>
        </form>
    );
};

export default AppUserChangePassword;