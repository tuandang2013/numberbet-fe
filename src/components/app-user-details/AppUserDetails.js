import React from 'react';

const AppUserDetails = ({ email, phone }) => (
    <div className="table-responsive">
        <table className="table">
            <tbody>
                <tr>
                    <td>
                        Email
                </td>
                    <td>
                        {email}
                    </td>
                </tr>
                <tr>
                    <td>
                        Điện thoại
                </td>
                    <td>
                        {phone}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
);

export default AppUserDetails;