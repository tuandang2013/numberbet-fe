import React from 'react';
import { useForm } from 'react-hook-form';
import AppFormError from '../app-form-error/AppFormError';
import { Form } from 'react-bootstrap';

const AppUserEdit = ({
    allClaims,
    email, phone, claims,
    onSubmit,
    updateError
}) => {

    const { handleSubmit, errors, register } = useForm({
        defaultValues: {
            email,
            phone,
            claims
        }
    });

    const formSubmit = data => onSubmit(data);

    return (
        <Form onSubmit={handleSubmit(formSubmit)}>
            <AppFormError title="Chỉnh sửa thất bại" error={updateError} />
            <div className="form-group">
                <label htmlFor="edit-email">Email</label>
                <input ref={register({ required: true })}
                    name="email"
                    type="email" className="form-control" id="edit-email"
                    aria-invalid={errors.email ? "true" : "false"}
                    aria-describedby="edit-email-help" />
                <small id="edit-email-help" className="form-text text-danger" style={{ display: errors.email ? "block" : "none" }} >
                    Vui lòng nhập email
                </small>
            </div>
            <div className="form-group">
                <label htmlFor="edit-phone">Số điện thoại</label>
                <input ref={register({ required: true })}
                    name="phone"
                    type="text" className="form-control" id="edit-phone"
                    aria-invalid={errors.phone ? "true" : "false"}
                    aria-describedby="edit-phone-help" />
                <small id="edit-phone-help" className="form-text text-danger" style={{ display: errors.email ? "block" : "none" }} >
                    Vui lòng nhập số điện thoại
                </small>
            </div>
            <div className="form-group">
                <label htmlFor="edit-claims">Quyền</label>
                <div>
                    {allClaims.map(claim =>
                        <Form.Check
                            ref={register()}
                            key={claim.id}
                            label={claim.name}
                            id={claim.id}
                            value={claim.name}
                            name="claims"
                            type="switch"
                        />
                    )}
                </div>
            </div>
            <button type="submit" className="btn btn-primary">Cập nhật</button>
        </Form>
    );
}

export default AppUserEdit;