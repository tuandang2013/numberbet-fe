import React from 'react';
import { Nav, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FaSignInAlt, FaSignOutAlt, FaHistory, FaUserCircle, FaUserPlus } from 'react-icons/fa';
import AppGoalSum from '../app-goal-sum/AppGoalSum';

const AppUserInfo = ({ signedIn, info: { email, goalPoint, testGoalPoint }, pathname }) => {
    function loggedIn() {
        return <Nav>
            <span data-testid='email' className="navbar-text">
                <FaUserCircle /><span className="d-inline d-md-none d-lg-inline">&nbsp;{email}</span>
            </span>
            <AppGoalSum signedIn={signedIn} info={{ goalPoint, testGoalPoint }} />
            <NavDropdown className={pathname.startsWith('/history') || pathname.startsWith('/goals') ? 'active' : ''} title={<span><FaHistory /> Lược sử</span>} id="basic-nav-dropdown">
                <NavDropdown.Item className={pathname.startsWith('/history') ? 'active' : ''} to="/history" as={Link}>Đặt số</NavDropdown.Item>
                <NavDropdown.Item className={pathname.startsWith('/goals') ? 'active' : ''} to="/goals" as={Link}>Điểm thưởng</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link to="/logout" as={Link} data-testid='logout-link'>
                <FaSignOutAlt /> Đăng xuất
            </Nav.Link>
        </Nav>;
    }

    function unloggedIn() {
        return (
            <Nav>
                <Nav.Link to="/login" className={pathname === '/login' ? 'active' : ''} as={Link} data-testid='login-link'><FaSignInAlt /> Đăng nhập</Nav.Link>
                <Nav.Link to="/register" className={pathname === '/register' ? 'active' : ''} as={Link} data-testid='register-link'><FaUserPlus /> Đăng ký</Nav.Link>
            </Nav>
        );
    }

    return (
        <>
            {signedIn ? loggedIn() : unloggedIn()}
        </>
    );
};

export default AppUserInfo;