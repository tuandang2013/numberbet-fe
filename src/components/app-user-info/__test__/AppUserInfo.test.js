import React from 'react';
import ReactDOM from 'react-dom';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import AppUserInfo from '../AppUserInfo';
import { StaticRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

const mockStore = configureMockStore();
const store = mockStore({ user: { info: {} } });

describe('AppUserInfo', () => {
    describe('Initialization', () => {
        test('should render', () => {
            const div = document.createElement('div');

            ReactDOM.render(
                <Provider store={store}>
                    <StaticRouter>
                        <AppUserInfo />
                    </StaticRouter>
                </Provider>, div);
        });
    });
});