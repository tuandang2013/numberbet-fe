import React, { useState, useCallback } from 'react';
import { Container, Row, Col, Modal, Button, Tabs, Tab } from 'react-bootstrap';
import AppPager from '../app-button-pager/AppButtonPager';
import { FaTrash, FaPen, FaEye, FaDollarSign, FaBtc, FaKey, FaHistory, FaHandHoldingUsd } from 'react-icons/fa';
import AppUserDetails from '../app-user-details/AppUserDetails';
import AppUserEdit from '../app-user-edit/AppUserEdit';
import AppUserChangePassword from '../app-user-change-password/AppUserChangePassword';
import AppHistoryList from '../app-history-list/AppHistoryList';
import AppGoalList from '../app-goal-list/AppGoalList';
import AppAdminCheckout from '../app-admin-checkout/AppAdminCheckout';

const AppUserList = ({
    allClaims, recordTypes,
    data, pager, onChangePage,
    onDeleteUser, onGetDetails, onUpdate, onChangePass,
    onGetUserRecords, onGetUserGoals, onGetUserGoalPoint, onGetUserTestGoalPoint,
    onCheckout, checkoutError, clearCheckoutError,
    updateError, changePassError,
    clearUpdateError, clearChangePassError
}) => {
    const [showDetails, setShowDetails] = useState(false);
    const [viewedUser, setViewedUser] = useState(null);

    const [showEdit, setShowEdit] = useState(false);
    const [editedUser, setEditedUser] = useState(null);

    const [showChangePass, setShowChangePass] = useState(false);
    const [changePassUser, setChangePassUser] = useState(null);

    const [showCheckout, setShowCheckout] = useState(false);
    const [checkoutUser, setCheckoutUser] = useState(null);

    const [showUserRecords, setShowUserRecords] = useState(false);
    const [userRecords, setUserRecords] = useState(null);

    const [showUserGoals, setShowUserGoals] = useState(false);
    const [userGoals, setUserGoals] = useState(null);
    const [userGoalPoint, setUserGoalPoint] = useState(null);
    const [userTestGoalPoint, setUserTestGoalPoint] = useState(null);

    const viewDetails = useCallback(user => {
        setViewedUser(user);
        setShowDetails(true);
    }, []);

    const hideDetails = useCallback(() => {
        setViewedUser(null);
        setShowDetails(false);
    }, []);

    const editDetails = useCallback(user => {
        clearUpdateError();
        setEditedUser(user);
        setShowEdit(true);
    }, [clearUpdateError]);

    const hideEdit = useCallback(() => {
        setEditedUser(null);
        setShowEdit(false);
    }, []);

    const changePass = useCallback(user => {
        clearChangePassError();
        setChangePassUser(user);
        setShowChangePass(true);
    }, [clearChangePassError]);

    const hideChangePass = useCallback(() => {
        setChangePassUser(null);
        setShowChangePass(false);
    }, []);

    const userCheckout = useCallback(user => {
        clearCheckoutError();
        setCheckoutUser(user);
        setShowCheckout(true);
    }, [clearCheckoutError]);

    const hideCheckout = useCallback(() => {
        setCheckoutUser(null);
        setShowCheckout(false);
    }, []);

    const viewRecords = useCallback(records => {
        setUserRecords(records);
        setShowUserRecords(true);
    }, []);

    const hideRecords = useCallback(() => {
        setUserRecords(null);
        setShowUserRecords(false);
    }, []);

    const viewUserGoals = useCallback(userGoals => {
        setUserGoals(userGoals);
        setUserGoalPoint(userGoals.data.goalPoint);
        setUserTestGoalPoint(userGoals.data.testGoalPoint);
        setShowUserGoals(true);
    }, []);

    const hideUserGoals = useCallback(() => {
        setUserGoals(null);
        setUserGoalPoint(null);
        setUserTestGoalPoint(null);
        setShowUserGoals(false);
    }, []);

    const onUserUpdate = useCallback(id => details => onUpdate(id, details, () => {
        onChangePage(pager.pageNumber);
        hideEdit();
    }), [hideEdit, onChangePage, onUpdate]);
    const onUserChangePass = useCallback(id => changePassData => onChangePass(id, changePassData, hideChangePass), [hideChangePass, onChangePass]);
    const onUserCheckout = useCallback(id =>
        amount => onCheckout(
            id,
            amount,
            () => {
                onChangePage(pager.pageNumber);
                hideCheckout();
            })
        , [hideCheckout, onChangePage, onCheckout]);
    const onRecordChangePage = useCallback(id => page => onGetUserRecords(id, page, (data) => setUserRecords(data)), [onGetUserRecords]);
    const onGoalPointChangePage = useCallback(id => page => onGetUserGoalPoint(id, { page }, data => setUserGoalPoint(data.data)), [onGetUserGoalPoint]);
    const onTestGoalPointChangePage = useCallback(id => page => onGetUserTestGoalPoint(id, { page }, data => setUserTestGoalPoint(data.data)), [onGetUserTestGoalPoint]);

    const handleViewDetails = user => onGetDetails(user._id, viewDetails);
    const handleEditDetails = user => onGetDetails(user._id, editDetails);
    const handleChangePassword = user => onGetDetails(user._id, changePass);
    const handleViewUserRecords = user => onGetUserRecords(user._id, 1, viewRecords);
    const handleViewUserGoals = user => onGetUserGoals(user._id, viewUserGoals);

    return (
        <Container>
            <Row>
                <Col>
                    <h1 className="display-2 mb-4">Tài khoản</h1>

                    <div className="table-responsive">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th className="text-right"><FaDollarSign /></th>
                                    <th className="text-right"><FaBtc /></th>
                                    <th className="text-right">Lần chơi</th>
                                    <th className="text-right w-auto"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.length === 0 ?
                                    <tr>
                                        <td colSpan="5">Hiện tại chưa có dữ liệu</td>
                                    </tr> :
                                    data.map((user) =>
                                        <tr key={user._id}>
                                            <td>
                                                {user.email}
                                            </td>
                                            <td className="text-right">{user.goalPoint}</td>
                                            <td className="text-right">{user.testGoalPoint}</td>
                                            <td className="text-right">{user.recordCount}</td>
                                            <td className="text-right w-auto">
                                                <div className="btn-group" role="group" aria-label="user-action">
                                                    <button type="button" title="Chi tiết" className="btn btn-light" onClick={() => handleViewDetails(user)}><FaEye /></button>
                                                    <button type="button" title="Thanh toán" className="btn btn-light" onClick={() => userCheckout(user)}><FaHandHoldingUsd /></button>
                                                    <button type="button" title="Điểm (thưởng)" className="btn btn-light" onClick={() => handleViewUserGoals(user)}><FaDollarSign /></button>
                                                    <button type="button" title="Đặt số" className="btn btn-light" onClick={() => handleViewUserRecords(user)}><FaHistory /></button>
                                                    <button type="button" title="Đổi mật khẩu" className="btn btn-light" onClick={() => handleChangePassword(user)}><FaKey /></button>
                                                    <button type="button" title="Chỉnh sửa" className="btn btn-light" onClick={() => handleEditDetails(user)}><FaPen /></button>
                                                    <button type="button" title="Xóa tài khoản" className="btn btn-light" onClick={() => onDeleteUser(user)}><FaTrash /></button>
                                                </div>
                                            </td>
                                        </tr>
                                    )}
                            </tbody>
                        </table>
                    </div>
                    <AppPager {...pager} onClick={onChangePage} />

                    {showDetails && viewedUser ? <Modal show={true} onHide={hideDetails}>
                        <Modal.Header closeButton>
                            <Modal.Title>Thông tin tài khoản</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <AppUserDetails {...viewedUser} />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={hideDetails}>Đóng</Button>
                        </Modal.Footer>
                    </Modal> : null}

                    {showEdit && editedUser ? <Modal show={true} onHide={hideEdit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Chỉnh sửa tài khoản</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <AppUserEdit {...editedUser} onSubmit={onUserUpdate(editedUser.id)} updateError={updateError} allClaims={allClaims} />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={hideEdit}>Đóng</Button>
                        </Modal.Footer>
                    </Modal> : null}

                    {showChangePass && changePassUser ? <Modal show={true} size="sm" onHide={hideChangePass}>
                        <Modal.Header closeButton>
                            <Modal.Title>Đổi mật khẩu</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <AppUserChangePassword {...changePassUser} onSubmit={onUserChangePass(changePassUser.id)} changePassError={changePassError} />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={hideChangePass}>Đóng</Button>
                        </Modal.Footer>
                    </Modal> : null}

                    {showCheckout && checkoutUser ? <Modal show={true} onHide={hideCheckout}>
                        <Modal.Header closeButton>
                            <Modal.Title>Thanh toán</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <AppAdminCheckout {...checkoutUser} onSubmit={onUserCheckout(checkoutUser._id)} checkoutError={checkoutError} />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={hideCheckout}>Đóng</Button>
                        </Modal.Footer>
                    </Modal> : null}

                    {showUserRecords && userRecords && recordTypes ? <Modal show={true} size="lg" onHide={hideRecords}>
                        <Modal.Header closeButton>
                            <Modal.Title>Đặt số</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <AppHistoryList
                                name='Người dùng'
                                histories={userRecords.histories.data}
                                pager={userRecords.histories.pager}
                                records={userRecords.entries}
                                recordTypes={recordTypes}
                                onClick={onRecordChangePage(userRecords.id)}
                            />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={hideRecords}>Đóng</Button>
                        </Modal.Footer>
                    </Modal> : null}

                    {showUserGoals && userGoals ? <Modal show={true} size="lg" onHide={hideUserGoals}>
                        <Modal.Header closeButton>
                            <Modal.Title>Điểm (thưởng)</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Tabs defaultActiveKey="goals">
                                <Tab eventKey="goals" title="Điểm">
                                    <AppGoalList {...userGoalPoint} onChangePage={onGoalPointChangePage(userGoals.id)} />
                                </Tab>
                                <Tab eventKey="test-goal" title="Điểm thưởng">
                                    <AppGoalList {...userTestGoalPoint} onChangePage={onTestGoalPointChangePage(userGoals.id)} />
                                </Tab>
                            </Tabs>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={hideUserGoals}>Đóng</Button>
                        </Modal.Footer>
                    </Modal> : null}
                </Col>
            </Row>
        </Container>
    );
};

export default AppUserList;