import React from 'react';
import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const signedInSelector = () => createSelector(
    store => store.user,
    user => user.signedIn
);

const Authenticate = ({ children, ...rest }) => {
    const signedIn = useSelector(signedInSelector());

    return (
        <Route {...rest}
            render={
                ({ location }) => signedIn ? children : <Redirect to={{ pathname: '/login', search: `redirectUrl=${encodeURIComponent(location.pathname)}`, state: { from: location } }} />
            }
        />
    );
};

export default Authenticate;