import React from 'react';
import Authenticate from '../Authenticate';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { MemoryRouter as Router, MemoryRouter } from 'react-router-dom';

const mockStore = configureMockStore();

describe('Authenticate', () => {
    it('should rendered', () => {
        const div = document.createElement('div');
        const store = mockStore({ user: { signedIn: true } });

        ReactDOM.render(
            <Provider store={store}>
                <MemoryRouter>
                    <Authenticate />
                </MemoryRouter>
            </Provider>, div
        );
    });
});