import React from 'react';
import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const signedInSelector = () => createSelector(
    store => store.user,
    user => user.signedIn
);

const claimsSelector = () => createSelector(
    store => store.user,
    user => user.info.claims
);

const appInitializedSelector = () => createSelector(
    store => store.app,
    app => app.initialized
);

const Authorize = ({ claims, children, ...rest }) => {
    const signedIn = useSelector(signedInSelector());
    const userClaims = useSelector(claimsSelector());
    const appInitialized = useSelector(appInitializedSelector());

    let forbidden = false;
    for (const claim of (claims || [])) {
        if (!userClaims.includes(claim)) {
            forbidden = true;
            break;
        }
    }

    return (
        appInitialized ?
            <Route {...rest}
                render={
                    ({ location }) => signedIn && !forbidden ? children : <Redirect to={{ pathname: '/forbidden', state: { from: location } }} />
                }
            /> : null
    );
};

export default Authorize;