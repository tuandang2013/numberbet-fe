import React from 'react';
import AppFooter from '../../components/app-footer/AppFooter';

const Footer = () => (
    <AppFooter />
);

export default Footer;