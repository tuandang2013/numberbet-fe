import React from 'react';
import AppForbidden from '../../components/app-forbidden/AppForbidden';

const Forbidden = () => {
    return (
        <AppForbidden/>
    )
};

export default Forbidden;