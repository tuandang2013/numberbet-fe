import React, { useEffect, useCallback } from 'react';
import { createSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import { processFetchGoals, processFetchGoalPoint, processFetchTestGoalPoint } from '../../redux/history/actions';
import AppGoals from '../../components/app-goals/AppGoals';
import { Helmet } from 'react-helmet';
import { GOAL_POINT, TEST_GOAL_POINT } from '../../contants/point.const';

const goalsSelector = () => createSelector(
    store => store.history,
    history => history.goals
);

const testGoalsSelector = () => createSelector(
    store => store.history,
    history => history.testGoals
);

const currentGoalSelector = () => createSelector(
    store => store.user,
    user => user.info.goalPoint
);

const currentTestGoalSelector = () => createSelector(
    store => store.user,
    user => user.info.testGoalPoint
);

const Goal = () => {
    const goals = useSelector(goalsSelector());
    const testGoals = useSelector(testGoalsSelector());

    const currentGoalPoint = useSelector(currentGoalSelector());
    const currentTestGoalPoint = useSelector(currentTestGoalSelector());

    const dispatch = useDispatch();

    const changePageType = useCallback(type => page => {
        switch (type) {
            case TEST_GOAL_POINT:
                dispatch(processFetchTestGoalPoint({ page }));
                break;
            case GOAL_POINT:
            default:
                dispatch(processFetchGoalPoint({ page }));
                break;

        }
    }, [dispatch]);

    useEffect(() => {
        dispatch(processFetchGoals());
    }, [dispatch]);

    return (
        <>
            <Helmet>
                <title>Điểm thưởng</title>
                <meta name="description" content="Lode.com điểm thưởng, nhận thưởng nhanh và uy tín" />
                <meta name="title" content="Lode.com điểm thưởng, nhận thưởng" />
                <meta name="keywords" content="Lode.com, điểm thưởng, nhận thưởng" />
            </Helmet>
            <AppGoals goals={goals} testGoals={testGoals} currentGoal={currentGoalPoint} currentTestGoal={currentTestGoalPoint} onChangePageType={changePageType} />
        </>
    );
};

export default Goal;