import React, { useEffect } from 'react';
import AppHistories from '../../components/app-histories/AppHistories';
import { createSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import { processFetchHistory } from '../../redux/history/actions';
import { info } from '../../redux/play/actions';
import { processFetchRecords } from '../../redux/play/record/actions';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router-dom';
import { SITE_URL } from '../../contants/site.const';

const historiesSelector = () => createSelector(
    store => store.history,
    history => history.histories
);

const playInfoInitSelector = () => createSelector(
    store => store.play.info,
    info => info.init
);

const recordTypesSelector = () => createSelector(
    store => store.play.info,
    info => info.types
);

const recordsSelector = () => createSelector(
    store => store.play.record,
    record => record.records
);

const History = () => {
    const playInfoInit = useSelector(playInfoInitSelector());
    const histories = useSelector(historiesSelector());
    const recordTypes = useSelector(recordTypesSelector());
    const records = useSelector(recordsSelector());

    const baseUrl = '/history';
    const { page } = useParams();

    const dispatch = useDispatch();

    useEffect(() => {
        if (!playInfoInit) {
            dispatch(info());
        } else {
            dispatch(processFetchHistory({ page }));
        }
    }, [playInfoInit, page, dispatch]);

    useEffect(() => {
        dispatch(processFetchRecords());
    }, [dispatch]);

    return (
        <>
            <Helmet>
                <title>History</title>
                <meta name="description" content="Lode.com lược sử đặt số, thông tin kết quả" />
                <meta name="title" content="Lode.com lược sử đặt số" />
                <meta name="keywords" content="Lode.com, lược sử, đặt số" />
                <link rel="canonical" href={`${SITE_URL}/history`} />
            </Helmet>
            <AppHistories histories={histories} records={records} recordTypes={recordTypes} baseUrl={baseUrl} />
        </>
    );
};

export default History;