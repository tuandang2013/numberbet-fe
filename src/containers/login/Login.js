import React, { useEffect } from 'react';
import AppLogin from '../../components/app-login/AppLogin';
import { useSelector, useDispatch } from 'react-redux';
import { createSelector } from 'reselect';
import { signIn } from '../../redux/user/actions';
import { useHistory, useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { LOGIN } from '../../redux/user/actionNames';
import { clearError } from '../../redux/app/actions';

const signedInSelector = () => createSelector(
    store => store.user,
    user => user.signedIn
);
const processingSelector = () => createSelector(
    store => store.app,
    app => app.processing
);
const errorSelector = () => createSelector(
    store => store.app.error,
    error => error[LOGIN]
);

const Login = () => {
    const signedIn = useSelector(signedInSelector());
    const processing = useSelector(processingSelector());
    const error = useSelector(errorSelector());
    const dispatch = useDispatch();
    const history = useHistory();

    const query = new URLSearchParams(useLocation().search);
    const redirectUrl = query.get('redirectUrl');

    useEffect(() => {
        dispatch(clearError(LOGIN));
    }, [dispatch]);

    useEffect(() => {
        if (signedIn) {
            if (redirectUrl) {
                history.replace(redirectUrl);
            } else {
                history.replace('/');
            }
        }
    }, [signedIn, history, redirectUrl]);

    function login(email, password, remember) {
        if (email == null)
            throw new Error('Email must be not null');
        if (password == null)
            throw new Error('Password must be not null');

        dispatch(signIn({ email, password, remember }));
    }

    return (
        <>
            <Helmet>
                <title>Đăng nhập</title>
                <meta name="description" content="Lode.com đăng nhập tài khoản" />
                <meta name="title" content="Lode.com đăng nhập" />
                <meta name="keywords" content="Lode.com, đăng nhập, tài khoản" />
            </Helmet>
            <AppLogin onSubmit={login} error={error} processing={processing} />
        </>
    );
};

export default Login;