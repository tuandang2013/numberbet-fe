import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import Login from '../Login';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { StaticRouter as Router } from 'react-router-dom';

Enzyme.configure({ adapter: new Adapter() });

const mockStore = configureMockStore();
const store = mockStore({ user: { info: {}, app: { login: {} } } });

describe('Login', () => {
    describe('Initialization', () => {
        test('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(
                <Provider store={store}>
                    <Router>
                        <Login />
                    </Router>
                </Provider>, div
            );
        });
    });
});