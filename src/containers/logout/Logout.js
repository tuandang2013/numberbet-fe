import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { logout } from '../../redux/user/actions';

const signedSelector = () => createSelector(
    store => store.user,
    user => user.signedIn
);

const Logout = () => {
    const signedIn = useSelector(signedSelector());
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        if (!signedIn) {
            history.replace('/');
        } else {
            dispatch(logout());
        }
    }, [signedIn, history, dispatch]);

    return null;
};

export default Logout;