import React, { useEffect, useCallback } from 'react';
import { createSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import { processAdminFetchUser, processAdminDeleteUser, processAdminGetUserDetails, processAdminUpdateUser, processAdminChangeUserPass } from '../../redux/admin/user/actions';
import AppUserList from '../../components/app-user-list/AppUserList';
import { ADMIN_UPDATE_USER, ADMIN_CHANGE_USERPASS } from '../../redux/admin/user/actionNames';
import { ADMIN_CHECKOUT } from '../../redux/admin/goal/actionNames';
import { clearError } from '../../redux/app/actions';
import { processFetchClaim } from '../../redux/admin/claim/actions';
import { processFetchUserRecord } from '../../redux/admin/record/actions';
import info from '../../redux/play/info/actions';
import { processFetchUserGoals, processFetchUserGoalPoint, processFetchUserTestGoalPoint, processAdminCheckout } from '../../redux/admin/goal/actions';

const usersSelector = createSelector(
    store => store.admin.user,
    user => user.users
);

const updateErrorSelector = createSelector(
    store => store.app,
    app => app.error[ADMIN_UPDATE_USER]
);

const changePassErrorSelector = createSelector(
    store => store.app,
    app => app.error[ADMIN_CHANGE_USERPASS]
);

const checkoutErrorSelector = createSelector(
    store => store.app,
    app => app.error[ADMIN_CHECKOUT]
);

const claimSelector = createSelector(
    store => store.admin.claim,
    claim => claim.claims
);

const recordTypesSelector = createSelector(
    store => store.play.info,
    info => info.types
);

const playInfoInitSelector = createSelector(
    store => store.play.info,
    info => info.init
);

const ManageUser = () => {
    const dispatch = useDispatch();

    const allClaims = useSelector(claimSelector);

    const playInit = useSelector(playInfoInitSelector);
    const recordTypes = useSelector(recordTypesSelector);

    const userData = useSelector(usersSelector);
    const updateError = useSelector(updateErrorSelector);
    const changePassError = useSelector(changePassErrorSelector);
    const checkoutError = useSelector(checkoutErrorSelector);

    useEffect(() => {
        dispatch(processAdminFetchUser());
    }, [dispatch]);

    useEffect(() => {
        if (!playInit) {
            dispatch(info());
        }
    }, [dispatch, playInit]);

    useEffect(() => {
        dispatch(processFetchClaim());
    }, [dispatch]);

    const changePage = useCallback((page) => {
        dispatch(processAdminFetchUser({ page }));
    }, [dispatch]);

    const getDetails = useCallback((id, onComplete) => {
        dispatch(processAdminGetUserDetails(id, onComplete));
    }, [dispatch]);

    const getUserRecords = useCallback((id, page, onComplete) => {
        dispatch(processFetchUserRecord(id, { page, size: 6 }, onComplete));
    }, [dispatch]);

    const getUserGoals = useCallback((id, onComplete) => {
        dispatch(processFetchUserGoals(id, { size: 6 }, onComplete));
    }, [dispatch]);

    const getUserGoalPoint = useCallback((id, { page, size = 6 }, onComplete) => {
        dispatch(processFetchUserGoalPoint(id, { page, size }, onComplete));
    }, [dispatch]);

    const getUserTestGoalPoint = useCallback((id, { page, size = 6 }, onComplete) => {
        dispatch(processFetchUserTestGoalPoint(id, { page, size }, onComplete));
    }, [dispatch]);

    const deleteUser = useCallback((user) => {
        if (window.confirm(`Are you sure delete ${user.email} user?`)) {
            dispatch(processAdminDeleteUser(user._id, () => changePage(1)));
        }
    }, [dispatch, changePage]);

    const updateUser = useCallback((id, details, onComplete) => {
        dispatch(processAdminUpdateUser(id, details, onComplete));
    }, [dispatch, changePage]);

    const checkout = useCallback((id, amount, onComplete) => {
        dispatch(processAdminCheckout(id, amount, onComplete));
    }, [dispatch]);

    const clearCheckoutError = useCallback(() => {
        dispatch(clearError(ADMIN_CHECKOUT));
    }, [dispatch]);

    const clearUpdateError = useCallback(() => {
        dispatch(clearError(ADMIN_UPDATE_USER));
    }, [dispatch]);

    const changePass = useCallback((id, changePassData, onComplete) => {
        dispatch(processAdminChangeUserPass(id, changePassData, onComplete));
    }, [dispatch]);

    const clearChangePassError = useCallback(() => {
        dispatch(clearError(ADMIN_CHANGE_USERPASS));
    }, [dispatch]);

    return (
        <AppUserList {...userData}
            allClaims={allClaims}
            onChangePage={changePage}
            onDeleteUser={deleteUser}
            onGetDetails={getDetails}
            onGetUserRecords={getUserRecords}
            onGetUserGoals={getUserGoals}
            onGetUserGoalPoint={getUserGoalPoint}
            onGetUserTestGoalPoint={getUserTestGoalPoint}
            onUpdate={updateUser}
            onCheckout={checkout}
            clearCheckoutError={clearCheckoutError}
            checkoutError={checkoutError}
            updateError={updateError}
            clearUpdateError={clearUpdateError}
            onChangePass={changePass}
            changePassError={changePassError}
            clearChangePassError={clearChangePassError}
            recordTypes={recordTypes}
        />
    );
};

export default ManageUser;