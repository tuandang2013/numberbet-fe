import React, { useEffect } from 'react';
import { useCallback } from 'react';
import { createSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import AppMessages from '../../components/app-messages/AppMessages';
import { removeMessage, clearMessage, clearNotErrorMessage } from '../../redux/message/actions';

const messageSelector = (tag) => createSelector(
    store => store.message,
    message => message.items.filter(msg => msg.tag === tag)
);

const delayTimeSelector = () => createSelector(
    store => store.message,
    message => message.time
);

const Message = ({ tag, keepError = false } = {}) => {
    const messages = useSelector(messageSelector(tag));
    const delay = useSelector(delayTimeSelector());
    const dispatch = useDispatch();

    useEffect(() => {
        if (messages.length !== 0 && (!keepError || messages.some(msg => !msg.error))) {
            const timeout = setTimeout(() => {
                dispatch(keepError ? clearNotErrorMessage() : clearMessage());
                clearTimeout(timeout);
            }, delay);
        }
    }, [messages, dispatch, delay, keepError]);

    const onRemove = useCallback(item => {
        dispatch(removeMessage(item));
    }, [dispatch]);

    return (
        <AppMessages messages={messages} onRemove={onRemove} />
    );
};

export default Message;