import React from 'react';
import AppNavbar from '../../components/app-navbar/AppNavbar';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { ADMINISTRATOR } from '../../contants/claim.const';

const userInfoSelector = createSelector(
    store => store.user,
    user => user
);

const isAdminSelector = createSelector(
    store => store.user.info,
    userInfo => (userInfo.claims || []).includes(ADMINISTRATOR)
)

const NavBar = () => {
    const { pathname } = useLocation();
    const isAdmin = useSelector(isAdminSelector);
    const user = useSelector(userInfoSelector);

    return (
        <AppNavbar pathname={pathname} user={user} isAdmin={isAdmin} />
    );
};

export default NavBar;