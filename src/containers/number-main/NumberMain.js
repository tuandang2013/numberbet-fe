import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AppNumber from '../../components/app-number/AppNumber';
import { fetchToday } from '../../redux/number/actions';
import { createSelector } from 'reselect';

const todaySelector = () => createSelector(
    store => store.numbers,
    numbers => numbers.today
);

const NumberMain = () => {
    const dispatch = useDispatch();
    const today = useSelector(todaySelector());

    useEffect(() => {
        dispatch(fetchToday());
    }, [dispatch]);

    return (
        <AppNumber {...today} />
    );
};

export default NumberMain;