import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import NumberMain from '../NumberMain';
import { Provider } from 'react-redux';

const mockStore = configureMockStore();
const store = mockStore({ app: { processing: false }, numbers: { today: {} } });

describe('NumberMain', () => {
    describe('Initialization', () => {
        it('should rendered', () => {
            const div = document.createElement('div');

            ReactDOM.render(
                <Provider store={store}>
                    <NumberMain />
                </Provider>, div);
        });
    });
});