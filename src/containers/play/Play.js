import React, { useCallback } from 'react';
import { useEffect } from 'react';
import { createSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import { info } from '../../redux/play/actions';
import { useParams, useHistory } from 'react-router-dom';
import AppPlay from '../../components/app-play/AppPlay';
import AppPlayArea from '../../components/app-play-area/AppPlayArea';
import { processCreateRecord } from '../../redux/play/record/actions';
import { notEnoughPointMessage } from '../../services/message.service';
import { clearTaggedMessage } from '../../redux/message/actions';
import { PLAY_MESSAGE_TAG } from '../../contants/message-tag.const';
import { SITE_URL } from '../../contants/site.const';
import { Helmet } from 'react-helmet';
import Message from '../message/Message';
import { CREATE_RECORD } from '../../redux/play/record/actionNames';
import { clearError } from '../../redux/app/actions';

const initSelector = () => createSelector(
    store => store.play.info,
    info => info.init
);

const recordInfoSelector = () => createSelector(
    store => store.play,
    play => play.info
);

const recordErrorSelector = () => createSelector(
    store => store.app.error,
    error => error[CREATE_RECORD]
);

const userPointsSelector = () => createSelector(
    store => store.user.info,
    info => ({
        goalPoint: info.goalPoint,
        testGoalPoint: info.testGoalPoint
    })
);

const Play = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { area } = useParams();

    const init = useSelector(initSelector());
    const recordInfo = useSelector(recordInfoSelector());
    const pointInfo = useSelector(userPointsSelector());
    const recordError = useSelector(recordErrorSelector());

    const addCannotPlayMessage = useCallback((type) => {
        dispatch(notEnoughPointMessage(type));
    }, [dispatch]);

    const clearCannotPlayMessage = useCallback(() => {
        dispatch(clearTaggedMessage(PLAY_MESSAGE_TAG));
    }, [dispatch]);

    const backToHome = useCallback(() => {
        history.push('/');
    }, [history]);

    const createRecord = useCallback((record) => {
        dispatch(processCreateRecord(record, backToHome));
    }, [dispatch, backToHome]);

    useEffect(() => {
        dispatch(clearError(CREATE_RECORD))
    }, [dispatch, area]);

    useEffect(() => {
        if (!init) {
            dispatch(info());
        }
    }, [init, dispatch]);

    if (!init) return null;

    return (
        <>
            <Helmet>
                <title>Đặt số</title>
                <meta name="description" content="Lode.com lô đề, đặt số, nhận thưởng nhanh và uy tín" />
                <meta name="title" content="Lode.com lô đề, đặt số" />
                <meta name="keywords" content="Lode.com, lô đề, đặt số, nhận thưởng" />
                <link rel="canonical" href={`${SITE_URL}/play`} />
            </Helmet>
            {area == null ?
                <AppPlay /> :
                <>
                    <Message tag={PLAY_MESSAGE_TAG} keepError={true} />
                    <AppPlayArea
                        area={area}
                        recordInfo={recordInfo}
                        pointInfo={pointInfo}
                        recordError={recordError}
                        onCannotPlay={addCannotPlayMessage}
                        onCanPlay={clearCannotPlayMessage}
                        onSubmit={createRecord} />
                </>
            }
        </>
    );
};

export default Play;