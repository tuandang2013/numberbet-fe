import React from 'react';
import ReactDOM from 'react-dom';
import Play from '../Play';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { StaticRouter as Router, MemoryRouter } from 'react-router-dom';
import AppPlayArea from '../../../components/app-play-area/AppPlayArea';
import { cleanup } from '@testing-library/react';

const mockStore = configureMockStore();
const store = mockStore({
    user: {
        signedIn: false
    },
    play: {
        info: {},
        init: true
    }
});

configure({ adapter: new Adapter() });

describe('Play', () => {
    afterEach(cleanup);

    it('should rendered', () => {
        const div = document.createElement('div');

        ReactDOM.render(<Provider store={store}>
            <Router>
                <Play />
            </Router>
        </Provider>, div);
    });

    it('should render area with default', () => {
        const wrapper = shallow(
            <MemoryRouter initialEntries={['/play']}>
                <Play />
            </MemoryRouter>
        );

        const playArea = wrapper.find(AppPlayArea);
        expect(playArea.length).toBe(1);
        expect(true).toBe(false);
    });
});