import React, { useEffect } from 'react';
import AppRegister from '../../components/app-register/AppRegister';
import { useSelector, useDispatch } from 'react-redux';
import { createSelector } from 'reselect';
import { registerRequest } from '../../redux/user/actions';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { REGISTER } from '../../redux/actionNames';
import { clearError } from '../../redux/app/actions';

const signedInSelector = () => createSelector(
    store => store.user,
    user => user.signedIn
);
const processingSelector = () => createSelector(
    store => store.app,
    app => app.processing
);
const errorSelector = () => createSelector(
    store => store.app.error,
    error => error[REGISTER]
);

const Register = () => {
    const signedIn = useSelector(signedInSelector());
    const processing = useSelector(processingSelector());
    const error = useSelector(errorSelector());
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(clearError(REGISTER));
    }, [dispatch]);

    useEffect(() => {
        if (signedIn) {
            history.replace('/');
        }
    }, [signedIn, history]);

    function register(email, password, confirmPassword, phone) {
        // if (email == null)
        //     throw new Error('Email must be not null');
        // if (password == null)
        //     throw new Error('Password must be not null');
        // if (confirmPassword == null)
        //     throw new Error('Confirm password must be not null');
        // else if (confirmPassword !== password)
        //     throw new Error('Confirm password must match password');
        // if (phone == null)
        //     throw new Error('Phone must be not null');

        dispatch(registerRequest({ email, password, phone, confirmPassword }));
    }

    return (
        <>
            <Helmet>
                <title>Đăng ký</title>
                <meta name="description" content="Lode.com đăng ký tài khoản" />
                <meta name="title" content="Lode.com đăng ký" />
                <meta name="keywords" content="Lode.com, đăng ký, tài khoản" />
            </Helmet>
            <AppRegister onSubmit={register} error={error} processing={processing} />
        </>
    );
};

export default Register;