import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Register from '../Register';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { StaticRouter as Router } from 'react-router-dom';

Enzyme.configure({ adapter: new Adapter() });

const mockStore = configureMockStore();
const store = mockStore({ user: { app: { register: {} } } });

describe('Register', () => {
    describe('Initialization', () => {
        test('should renderred', () => {
            const div = document.createElement('div');

            ReactDOM.render(
                <Provider store={store}>
                    <Router>
                        <Register />
                    </Router>
                </Provider>, div);
        });
    });
});