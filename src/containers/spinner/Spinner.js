import React from 'react';
import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import AppSpinner from '../../components/app-spinner/AppSpinner';

const showWaitingSelector = () => createSelector(
    store => store.app,
    app => app.processing && app.waiting
);

const Spinner = () => {
    const show = useSelector(showWaitingSelector());

    return (
        show ? <AppSpinner /> : null
    );
};

export default Spinner;