const GUEST = 'Guest';
const REGISTERED = 'Registered';
const ADMINISTRATOR = 'Administrator';

export { GUEST, REGISTERED, ADMINISTRATOR };