const GLOBAL_MESSAGE_TAG = '@app/message/tag/global';
const PLAY_MESSAGE_TAG = '@app/message/tag/play';

export { PLAY_MESSAGE_TAG, GLOBAL_MESSAGE_TAG };