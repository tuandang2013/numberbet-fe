const SITE_NAME = 'Lode.com';
const SITE_URL = 'http://lode.com';

const META_DESCRIPTION = 'Lode.com cung cấp thông tin xổ số, lô đề nhanh và uy tín';
const META_TITLE = 'Lode.com xổ số, lô đề nhanh và uy tín';
const META_KEYWORD = 'Lode.com, xổ số, lô đề';

export { SITE_NAME, META_DESCRIPTION, META_TITLE, META_KEYWORD, SITE_URL };