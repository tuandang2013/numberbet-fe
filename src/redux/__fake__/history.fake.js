const historyFakeData = [{
    entry: {
        area: 'area',
        channel: 'channel',
        number: 'number',
        point: 15,
        pointType: 'goal-point',
        type: '2 numbers',
        date: '2020-03-17'
    },
    success: false,
    failedReason: 'Wrong number',
    source: null,
    createdDate: new Date('2020-03-18')
}, {
    entry: {
        area: 'area 2',
        channel: 'channel 2',
        number: 'number 2',
        point: 16,
        pointType: 'test-goal-point',
        type: '@test/three-number',
        date: '2020-03-16'
    },
    success: false,
    failedReason: 'Wrong number',
    source: null,
    createdDate: new Date('2020-03-16')
}, {
    entry: {
        area: 'area 3',
        channel: 'channel 3',
        number: 'number 3',
        point: 15,
        pointType: 'test-goal-point',
        type: '@test/two-number',
        date: '2020-03-14'
    },
    success: false,
    failedReason: 'Wrong number',
    source: null,
    createdDate: new Date('2020-03-15')
}];

export default historyFakeData;