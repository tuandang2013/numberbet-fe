const playInfoFakeData = {
    init: true,
    areas: [{
        name: 'Miền Bắc',
        code: 'north',
        channels: [
            'Quảng Ninh'
        ]
    }, {
        name: 'Miền Nam',
        code: 'south',
        channels: [
            'Bạc Liêu',
            'Bến Tre',
            'Vũng Tàu'
        ]
    }],
    types: [{
        info: {
            name: 'test 2 số',
            code: '@test/two-number',
            description: 'Test bao lô dọc 2 số'
        },
        pattern: '^\\d{2}$',
        pointRate: {
            north: 10,
            south: 50000
        },
        numberCount: 2
    }, {
        info: {
            name: 'test 3 số',
            code: '@test/three-number',
            description: ''
        },
        pattern: '^\\d{3}$',
        pointRate: {
            north: 50000,
            south: 100
        },
        numberCount: 3
    }],
    pointTypes: [
        'test-point',
        'test-test-point'
    ],
    date: '2020-03-16T17:00:00.000Z'
};

export default playInfoFakeData;