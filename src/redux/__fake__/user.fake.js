const userFakeData = {
    signedIn: true,
    info: {
        email: 'test@user.com',
        goalPoint: 10,
        testGoalPoint: 100,
        claims: []
    },
    token: 'token'
};

export default userFakeData;