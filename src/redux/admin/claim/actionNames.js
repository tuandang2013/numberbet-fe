const FETCH_CLAIM = '@app/admin/claim/FETCH_CLAIM';
const FETCH_CLAIM_FAILED = '@app/admin/claim/FETCH_CLAIM_FAILED';
const FETCH_CLAIM_SUCCESS = '@app/admin/claim/FETCH_CLAIM_SUCCESS';


export {
    FETCH_CLAIM,
    FETCH_CLAIM_FAILED,
    FETCH_CLAIM_SUCCESS
};