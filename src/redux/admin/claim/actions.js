import {
    FETCH_CLAIM, FETCH_CLAIM_FAILED, FETCH_CLAIM_SUCCESS
} from './actionNames';
import { getClaims } from '../../../services/claim.service';

const fetchClaim = () => ({
    type: FETCH_CLAIM
});

const fetchClaimFailed = (error) => ({
    type: FETCH_CLAIM_FAILED,
    payload: error
});

const fetchClaimSuccess = (data) => ({
    type: FETCH_CLAIM_SUCCESS,
    payload: data
});

const processFetchClaim = () => dispatch => {
    dispatch(fetchClaim());
    return getClaims().then(data => dispatch(fetchClaimSuccess(data)))
        .catch(error => dispatch(fetchClaimFailed(error)));
};


export {
    processFetchClaim
};