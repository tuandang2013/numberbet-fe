import { FETCH_CLAIM, FETCH_CLAIM_SUCCESS, FETCH_CLAIM_FAILED } from "./actionNames";

const initialState = {
    claims: []
};

const claimReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CLAIM:
        case FETCH_CLAIM_FAILED:
            return state;
        case FETCH_CLAIM_SUCCESS:
            return {
                ...state,
                claims: action.payload
            };
        default:
            return state;
    }
};

export default claimReducer;