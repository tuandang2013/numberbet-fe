import {
    FETCH_USER_GOALS, FETCH_USER_GOALS_FAILED, FETCH_USER_GOALS_SUCCESS,
    FETCH_USER_GOALPOINT_SUCCESS, FETCH_USER_GOALPOINT_FAILED, FETCH_USER_GOALPOINT,
    FETCH_USER_TESTGOALPOINT_SUCCESS, FETCH_USER_TESTGOALPOINT_FAILED, FETCH_USER_TESTGOALPOINT, ADMIN_CHECKOUT, ADMIN_CHECKOUT_SUCCESS, ADMIN_CHECKOUT_FAILED
} from './actionNames';
import { getUserGoals, getUserGoalPoint, getUserTestGoalPoint, checkoutForUser } from '../../../services/goals.service';

const fetchUserGoals = () => ({
    type: FETCH_USER_GOALS
});

const fetchUserGoalsSuccess = (data) => ({
    type: FETCH_USER_GOALS_SUCCESS,
    payload: data
});

const fetchUserGoalFailed = (error) => ({
    type: FETCH_USER_GOALS_FAILED,
    payload: error
});

const processFetchUserGoals = (id, opts, onComplete) => dispatch => {
    dispatch(fetchUserGoals());
    return getUserGoals(id, opts).then(data => {
        dispatch(fetchUserGoalsSuccess({ id, data }));
        return { id, data };
    }).then(onComplete).catch(error => dispatch(fetchUserGoalFailed(error)));
};

const fetchUserGoalPoint = () => ({
    type: FETCH_USER_GOALPOINT
});

const fetchUserGoalPointSuccess = (data) => ({
    type: FETCH_USER_GOALPOINT_SUCCESS,
    payload: data
});

const fetchUserGoalPointFailed = (error) => ({
    type: FETCH_USER_GOALPOINT_FAILED,
    payload: error
});

const processFetchUserGoalPoint = (id, opts, onComplete) => dispatch => {
    dispatch(fetchUserGoalPoint());
    return getUserGoalPoint(id, opts).then(data => {
        dispatch(fetchUserGoalPointSuccess({ id, data }));
        return { id, data };
    }).then(onComplete).catch(error => dispatch(fetchUserGoalPointFailed(error)));
};

const fetchUserTestGoalPoint = () => ({
    type: FETCH_USER_TESTGOALPOINT
});

const fetchUserTestGoalPointSuccess = (data) => ({
    type: FETCH_USER_TESTGOALPOINT_SUCCESS,
    payload: data
});

const fetchUserTestGoalPointFailed = (error) => ({
    type: FETCH_USER_TESTGOALPOINT_FAILED,
    payload: error
});

const processFetchUserTestGoalPoint = (id, opts, onComplete) => dispatch => {
    dispatch(fetchUserTestGoalPoint());
    return getUserTestGoalPoint(id, opts).then(data => {
        dispatch(fetchUserTestGoalPointSuccess({ id, data }));
        return { id, data };
    }).then(onComplete).catch(error => dispatch(fetchUserTestGoalPointFailed(error)));
};

const adminCheckout = () => ({
    type: ADMIN_CHECKOUT
});

const adminCheckoutSuccess = (data) => ({
    type: ADMIN_CHECKOUT_SUCCESS,
    payload: data
});

const adminCheckoutFailed = (error) => ({
    type: ADMIN_CHECKOUT_FAILED,
    payload: error
});

const processAdminCheckout = (id, amount, onComplete) => dispatch => {
    dispatch(adminCheckout());
    return checkoutForUser(id, { amount }).then(data => {
        dispatch(adminCheckoutSuccess({ id, data }));
        return { id, data };
    }).then(onComplete).catch(error => dispatch(adminCheckoutFailed(error)));
};

export {
    processFetchUserGoals,
    processFetchUserGoalPoint,
    processFetchUserTestGoalPoint,
    processAdminCheckout
};