import {
    FETCH_USER_GOALS, FETCH_USER_GOALS_FAILED, FETCH_USER_GOALS_SUCCESS,
    FETCH_USER_GOALPOINT, FETCH_USER_GOALPOINT_FAILED, FETCH_USER_GOALPOINT_SUCCESS,
    FETCH_USER_TESTGOALPOINT, FETCH_USER_TESTGOALPOINT_FAILED, FETCH_USER_TESTGOALPOINT_SUCCESS,
    ADMIN_CHECKOUT_SUCCESS, ADMIN_CHECKOUT_FAILED, ADMIN_CHECKOUT
} from './actionNames';

const initialState = {
    goal: {},
    goalPoint: {},
    testGoalPoint: {},
    checkout: {}
};

const adminGoalReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_GOALS:
        case FETCH_USER_GOALS_FAILED:
            return state;
        case FETCH_USER_GOALS_SUCCESS:
            return {
                ...state,
                goal: {
                    ...state.goal,
                    [action.payload.id]: action.payload.data
                }
            };
        case FETCH_USER_GOALPOINT:
        case FETCH_USER_GOALPOINT_FAILED:
            return state;
        case FETCH_USER_GOALPOINT_SUCCESS:
            return {
                ...state,
                goalPoint: {
                    ...state.goal,
                    [action.payload.id]: action.payload.data
                }
            };
        case FETCH_USER_TESTGOALPOINT:
        case FETCH_USER_TESTGOALPOINT_FAILED:
            return state;
        case FETCH_USER_TESTGOALPOINT_SUCCESS:
            return {
                ...state,
                testGoalPoint: {
                    ...state.goal,
                    [action.payload.id]: action.payload.data
                }
            };
        case ADMIN_CHECKOUT:
        case ADMIN_CHECKOUT_FAILED:
            return state;
        case ADMIN_CHECKOUT_SUCCESS:
            return {
                ...state,
                checkout: {
                    ...state.checkout,
                    [action.payload.id]: action.payload.data
                }
            };
        default:
            return state;
    }
};

export default adminGoalReducer;