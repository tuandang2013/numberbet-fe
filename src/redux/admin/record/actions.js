import {
    FETCH_USER_RECORDS, FETCH_USER_RECORDS_FAILED, FETCH_USER_RECORDS_SUCCESS
} from './actionNames';
import { getUserRecords } from '../../../services/record.service';

const fetchUserRecord = () => ({
    type: FETCH_USER_RECORDS
});

const fetchUserRecordFailed = (error) => ({
    type: FETCH_USER_RECORDS_FAILED,
    payload: error
});

const fetchUserRecordSuccess = (id, data) => ({
    type: FETCH_USER_RECORDS_SUCCESS,
    payload: { id, data }
});

const processFetchUserRecord = (id, opts, onComplete) => dispatch => {
    dispatch(fetchUserRecord());
    return getUserRecords(id, opts).then(data => {
        dispatch(fetchUserRecordSuccess(id, data));
        return data;
    })
        .then(onComplete)
        .catch(error => dispatch(fetchUserRecordFailed(error)));
};

export {
    processFetchUserRecord
}