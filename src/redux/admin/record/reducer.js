import {
    FETCH_USER_RECORDS, FETCH_USER_RECORDS_FAILED, FETCH_USER_RECORDS_SUCCESS
} from './actionNames';

const initialState = {
    record: {}
};

const adminRecordReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_RECORDS:
        case FETCH_USER_RECORDS_FAILED:
            return state;
        case FETCH_USER_RECORDS_SUCCESS:
            return {
                ...state,
                record: {
                    ...state.record,
                    [action.payload.id]: action.payload.data
                }
            };
        default:
            return state;
    }
};

export default adminRecordReducer;