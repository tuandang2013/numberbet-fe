import { combineReducers } from 'redux';
import userReducer from './user/reducer';
import claimReducer from './claim/reducer';
import recordReducer from './record/reducer';
import goalReducer from './goal/reducer';

const adminReducer = combineReducers({
    user: userReducer,
    claim: claimReducer,
    record: recordReducer,
    goal: goalReducer
});

export default adminReducer;