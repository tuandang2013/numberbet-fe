import {
    ADMIN_FETCH_USERS_FAILED, ADMIN_FETCH_USERS_SUCCESS, ADMIN_FETCH_USERS,
    ADMIN_DELETE_USER, ADMIN_DELETE_USER_FAILED, ADMIN_DELETE_USER_SUCCESS,
    ADMIN_GET_USERDETAILS, ADMIN_GET_USERDETAILS_FAILED, ADMIN_GET_USERDETAILS_SUCCESS,
    ADMIN_UPDATE_USER, ADMIN_UPDATE_USER_FAILED, ADMIN_UPDATE_USER_SUCCESS,
    ADMIN_CHANGE_USERPASS, ADMIN_CHANGE_USERPASS_SUCCESS, ADMIN_CHANGE_USERPASS_FAILED
} from "./actionNames";
import { getUsers, deleteUser, getUserDetails, updateUser, changeUserPass } from '../../../services/user.service';

const adminFetchUser = () => ({
    type: ADMIN_FETCH_USERS
});

const adminFetchUserFailed = (error) => ({
    type: ADMIN_FETCH_USERS_FAILED,
    payload: error
});

const adminFetchUserSuccess = (data) => ({
    type: ADMIN_FETCH_USERS_SUCCESS,
    payload: data
});

const processAdminFetchUser = (opts) => dispatch => {
    dispatch(adminFetchUser());
    return getUsers(opts).then(data => dispatch(adminFetchUserSuccess(data)))
        .catch(error => dispatch(adminFetchUserFailed(error)));
};

const adminDeleteUser = () => ({
    type: ADMIN_DELETE_USER
});

const adminDeleteUserFailed = (error) => ({
    type: ADMIN_DELETE_USER_FAILED,
    payload: error
});

const adminDeleteUserSuccess = () => ({
    type: ADMIN_DELETE_USER_SUCCESS,
});

const processAdminDeleteUser = (id, onComplete) => dispatch => {
    dispatch(adminDeleteUser());
    return deleteUser(id).then(() => dispatch(adminDeleteUserSuccess()))
        .then(onComplete)
        .catch(error => dispatch(adminDeleteUserFailed(error)));
};

const adminGetUserDetails = () => ({
    type: ADMIN_GET_USERDETAILS
});

const adminGetUserDetailsFailed = (error) => ({
    type: ADMIN_GET_USERDETAILS_FAILED,
    payload: error
});

const adminGetUserDetailsSuccess = (data) => ({
    type: ADMIN_GET_USERDETAILS_SUCCESS,
    payload: data
});

const processAdminGetUserDetails = (id, onComplete) => dispatch => {
    dispatch(adminGetUserDetails());
    return getUserDetails(id)
        .then(data => {
            dispatch(adminGetUserDetailsSuccess({ id, data }));
            return data;
        })
        .then(onComplete)
        .catch(error => dispatch(adminGetUserDetailsFailed(error)));
};

const adminUpdateUser = () => ({
    type: ADMIN_UPDATE_USER
});

const adminUpdateUserFailed = (error) => ({
    type: ADMIN_UPDATE_USER_FAILED,
    payload: error
});

const adminUpdateUserSuccess = (data) => ({
    type: ADMIN_UPDATE_USER_SUCCESS,
    payload: data
});

const processAdminUpdateUser = (id, details, onComplete) => dispatch => {
    dispatch(adminUpdateUser());
    return updateUser(id, details)
        .then(({ data }) => {
            dispatch(adminUpdateUserSuccess({ id, data }));
            return data;
        })
        .then(onComplete)
        .catch(error => dispatch(adminUpdateUserFailed(error)));
};

const adminChangeUserPass = () => ({
    type: ADMIN_CHANGE_USERPASS
});

const adminChangeUserPassFailed = (error) => ({
    type: ADMIN_CHANGE_USERPASS_FAILED,
    payload: error
});

const adminChangeUserPassSuccess = () => ({
    type: ADMIN_CHANGE_USERPASS_SUCCESS,
});

const processAdminChangeUserPass = (id, changePassData, onComplete) => dispatch => {
    dispatch(adminChangeUserPass());
    return changeUserPass(id, changePassData)
        .then(() => {
            dispatch(adminChangeUserPassSuccess());
            return null;
        })
        .then(onComplete)
        .catch(error => dispatch(adminChangeUserPassFailed(error)));
};
export {
    adminFetchUser, adminFetchUserFailed, adminFetchUserSuccess, processAdminFetchUser,
    adminDeleteUser, adminDeleteUserFailed, adminDeleteUserSuccess, processAdminDeleteUser,
    processAdminGetUserDetails,
    processAdminUpdateUser,
    processAdminChangeUserPass
};