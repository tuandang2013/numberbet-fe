import {
    ADMIN_FETCH_USERS, ADMIN_FETCH_USERS_FAILED, ADMIN_FETCH_USERS_SUCCESS,
    ADMIN_DELETE_USER, ADMIN_DELETE_USER_SUCCESS, ADMIN_DELETE_USER_FAILED,
    ADMIN_GET_USERDETAILS, ADMIN_GET_USERDETAILS_FAILED, ADMIN_GET_USERDETAILS_SUCCESS,
    ADMIN_UPDATE_USER, ADMIN_UPDATE_USER_FAILED, ADMIN_UPDATE_USER_SUCCESS,
    ADMIN_CHANGE_USERPASS, ADMIN_CHANGE_USERPASS_FAILED, ADMIN_CHANGE_USERPASS_SUCCESS
} from "./actionNames";

const initialState = {
    users: {
        data: [],
        pager: {}
    },
    details: {}
};

const adminUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADMIN_FETCH_USERS:
        case ADMIN_FETCH_USERS_FAILED:
            return state;
        case ADMIN_FETCH_USERS_SUCCESS:
            return {
                ...state,
                users: action.payload
            };
        case ADMIN_DELETE_USER:
        case ADMIN_DELETE_USER_SUCCESS:
        case ADMIN_DELETE_USER_FAILED:
            return state;
        case ADMIN_GET_USERDETAILS:
        case ADMIN_GET_USERDETAILS_FAILED:
            return state;
        case ADMIN_GET_USERDETAILS_SUCCESS:
            return {
                ...state,
                details: {
                    ...state.details,
                    [action.payload.id]: action.payload.data
                }
            };
        case ADMIN_UPDATE_USER:
        case ADMIN_UPDATE_USER_FAILED:
            return state;
        case ADMIN_UPDATE_USER_SUCCESS:
            return {
                ...state,
                details: {
                    ...state.details,
                    [action.payload.id]: action.payload.data
                }
            };
        case ADMIN_CHANGE_USERPASS:
        case ADMIN_CHANGE_USERPASS_FAILED:
        case ADMIN_CHANGE_USERPASS_SUCCESS:
            return state;
        default:
            return state;
    }
};

export default adminUserReducer;