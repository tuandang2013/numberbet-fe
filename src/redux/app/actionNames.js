const APP_START = '@app/app/APP_START';
const APP_START_FAILED = '@app/app/APP_START_FAILED';
const APP_START_SUCCESS = '@app/app/APP_START_SUCCESS';

const CLEAR_ERROR = '@app/error/CLEAR_ERROR';

const CSRF_TOKEN = '@app/app/CSRF_TOKEN';

export { APP_START, APP_START_FAILED, APP_START_SUCCESS };
export { CLEAR_ERROR };
export { CSRF_TOKEN };