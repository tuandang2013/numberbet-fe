import {
    APP_START, APP_START_FAILED, APP_START_SUCCESS,
    CLEAR_ERROR,
    CSRF_TOKEN
} from './actionNames';

const appStart = () => ({ type: APP_START });

const appStartFailed = (error) => ({ type: APP_START_FAILED, payload: error });

const appStartSuccess = () => ({ type: APP_START_SUCCESS });

const clearError = (name) => ({ type: CLEAR_ERROR, payload: name });

const csrfToken = (token) => ({ type: CSRF_TOKEN, payload: token });

export {
    appStart, appStartFailed, appStartSuccess,
    csrfToken, clearError
};