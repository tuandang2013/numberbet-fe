import {
    APP_START, APP_START_FAILED, APP_START_SUCCESS,
    CSRF_TOKEN,
    CLEAR_ERROR
} from './actionNames';
import {
    FETCH_HISTORY,
    FETCH_HISTORY_FAILED,
    FETCH_HISTORY_SUCCESS,

    FETCH_GOALS,
    FETCH_GOALS_FAILED,
    FETCH_GOALS_SUCCESS,

    FETCH_GOALPOINT,
    FETCH_GOALPOINT_FAILED,
    FETCH_GOALPOINT_SUCCESS,

    FETCH_TESTGOALPOINT,
    FETCH_TESTGOALPOINT_FAILED,
    FETCH_TESTGOALPOINT_SUCCESS,

    FETCH_TODAY,
    FETCH_TODAY_FAILED,
    FETCH_TODAY_SUCCESS,

    FETCH_INFO,
    FETCH_INFO_FAILED,
    FETCH_INFO_SUCCESS,

    CREATE_RECORD,
    CREATE_RECORD_FAILED,
    CREATE_RECORD_SUCCESS,

    FETCH_RECORDS,
    FETCH_RECORDS_FAILED,
    FETCH_RECORDS_SUCCESS,

    LOGIN,
    LOGIN_FAILED,
    LOGIN_SUCCESS,

    REGISTER,
    REGISTER_FAILED,
    REGISTER_SUCCESS,

    ADMIN_FETCH_USERS,
    ADMIN_FETCH_USERS_FAILED,
    ADMIN_FETCH_USERS_SUCCESS,

    ADMIN_DELETE_USER,
    ADMIN_DELETE_USER_FAILED,
    ADMIN_DELETE_USER_SUCCESS,

    ADMIN_GET_USERDETAILS,
    ADMIN_GET_USERDETAILS_FAILED,
    ADMIN_GET_USERDETAILS_SUCCESS,

    ADMIN_UPDATE_USER,
    ADMIN_UPDATE_USER_FAILED,
    ADMIN_UPDATE_USER_SUCCESS,

    ADMIN_CHANGE_USERPASS,
    ADMIN_CHANGE_USERPASS_FAILED,
    ADMIN_CHANGE_USERPASS_SUCCESS,

    FETCH_CLAIM_SUCCESS,
    FETCH_CLAIM_FAILED,
    FETCH_CLAIM,

    FETCH_USER_RECORDS,
    FETCH_USER_RECORDS_FAILED,
    FETCH_USER_RECORDS_SUCCESS,

    FETCH_USER_GOALS,
    FETCH_USER_GOALS_FAILED,
    FETCH_USER_GOALS_SUCCESS,

    FETCH_USER_GOALPOINT,
    FETCH_USER_GOALPOINT_FAILED,
    FETCH_USER_GOALPOINT_SUCCESS,

    FETCH_USER_TESTGOALPOINT,
    FETCH_USER_TESTGOALPOINT_FAILED,
    FETCH_USER_TESTGOALPOINT_SUCCESS,

    ADMIN_CHECKOUT,
    ADMIN_CHECKOUT_FAILED,
    ADMIN_CHECKOUT_SUCCESS
} from '../actionNames';

const initialState = {
    processing: false,
    waiting: false,
    error: {},
    csrf: null,
    initialized: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        //app start
        case APP_START:
            return {
                ...state,
                processing: true,
                waiting: true,
                error: {
                    ...state.error,
                    [APP_START]: null
                }
            };
        case APP_START_SUCCESS:
            return {
                ...state,
                processing: false,
                waiting: false,
                initialized: true,
                error: {
                    ...state.error,
                    [APP_START]: null
                }
            };
        case APP_START_FAILED:
            return {
                ...state,
                processing: false,
                waiting: false,
                initialized: false,
                error: {
                    ...state.error,
                    [APP_START]: action.payload
                }
            };

        //fetch history
        case FETCH_HISTORY:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_HISTORY]: null
                }
            };
        case FETCH_HISTORY_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_HISTORY]: null
                }
            };
        case FETCH_HISTORY_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_HISTORY]: action.payload
                }
            };

        //fetch goals
        case FETCH_GOALS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_GOALS]: null
                }
            };
        case FETCH_GOALS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_GOALS]: null
                }
            };
        case FETCH_GOALS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_GOALS]: action.payload
                }
            };

        //fetch goals
        case FETCH_GOALPOINT:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_GOALPOINT]: null
                }
            };
        case FETCH_GOALPOINT_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_GOALPOINT]: null
                }
            };
        case FETCH_GOALPOINT_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_GOALPOINT]: action.payload
                }
            };

        //fetch test goals
        case FETCH_TESTGOALPOINT:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_TESTGOALPOINT]: null
                }
            };
        case FETCH_TESTGOALPOINT_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_TESTGOALPOINT]: null
                }
            };
        case FETCH_TESTGOALPOINT_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_TESTGOALPOINT]: action.payload
                }
            };

        //fetch test goals
        case FETCH_TODAY:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_TODAY]: null
                }
            };
        case FETCH_TODAY_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_TODAY]: null
                }
            };
        case FETCH_TODAY_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_TODAY]: action.payload
                }
            };

        //fetch play info
        case FETCH_INFO:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_INFO]: null
                }
            };
        case FETCH_INFO_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_INFO]: null
                }
            };
        case FETCH_INFO_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_INFO]: action.payload
                }
            };

        //create record
        case CREATE_RECORD:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [CREATE_RECORD]: null
                }
            };
        case CREATE_RECORD_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [CREATE_RECORD]: null
                }
            };
        case CREATE_RECORD_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [CREATE_RECORD]: action.payload
                }
            };

        //fetch records
        case FETCH_RECORDS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_RECORDS]: null
                }
            };
        case FETCH_RECORDS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_RECORDS]: null
                }
            };
        case FETCH_RECORDS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_RECORDS]: action.payload
                }
            };

        //login
        case LOGIN:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [LOGIN]: null
                }
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [LOGIN]: null
                }
            };
        case LOGIN_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [LOGIN]: action.payload
                }
            };

        //register
        case REGISTER:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [REGISTER]: null
                }
            };
        case REGISTER_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [REGISTER]: null
                }
            };
        case REGISTER_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [REGISTER]: action.payload
                }
            };

        //admin fetch users
        case ADMIN_FETCH_USERS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [ADMIN_FETCH_USERS]: null
                }
            };
        case ADMIN_FETCH_USERS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_FETCH_USERS]: null
                }
            };
        case ADMIN_FETCH_USERS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_FETCH_USERS]: action.payload
                }
            };

        //admin delete user
        case ADMIN_DELETE_USER:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [ADMIN_DELETE_USER]: null
                }
            };
        case ADMIN_DELETE_USER_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_DELETE_USER]: null
                }
            };
        case ADMIN_DELETE_USER_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_DELETE_USER]: action.payload
                }
            };

        //admin get user details
        case ADMIN_GET_USERDETAILS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [ADMIN_GET_USERDETAILS]: null
                }
            };
        case ADMIN_GET_USERDETAILS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_GET_USERDETAILS]: null
                }
            };
        case ADMIN_GET_USERDETAILS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_GET_USERDETAILS]: action.payload
                }
            };

        //admin update user
        case ADMIN_UPDATE_USER:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [ADMIN_UPDATE_USER]: null
                }
            };
        case ADMIN_UPDATE_USER_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_UPDATE_USER]: null
                }
            };
        case ADMIN_UPDATE_USER_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_UPDATE_USER]: action.payload
                }
            };

        //admin change user pass
        case ADMIN_CHANGE_USERPASS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [ADMIN_CHANGE_USERPASS]: null
                }
            };
        case ADMIN_CHANGE_USERPASS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_CHANGE_USERPASS]: null
                }
            };
        case ADMIN_CHANGE_USERPASS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_CHANGE_USERPASS]: action.payload
                }
            };

        //admin fetch claim
        case FETCH_CLAIM:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_CLAIM]: null
                }
            };
        case FETCH_CLAIM_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_CLAIM]: null
                }
            };
        case FETCH_CLAIM_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_CLAIM]: action.payload
                }
            };

        //admin fetch user record
        case FETCH_USER_RECORDS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_USER_RECORDS]: null
                }
            };
        case FETCH_USER_RECORDS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_RECORDS]: null
                }
            };
        case FETCH_USER_RECORDS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_RECORDS]: action.payload
                }
            };

        //admin fetch user goals
        case FETCH_USER_GOALS:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_USER_GOALS]: null
                }
            };
        case FETCH_USER_GOALS_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_GOALS]: null
                }
            };
        case FETCH_USER_GOALS_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_GOALS]: action.payload
                }
            };

        //admin fetch user goal point
        case FETCH_USER_GOALPOINT:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_USER_GOALPOINT]: null
                }
            };
        case FETCH_USER_GOALPOINT_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_GOALPOINT]: null
                }
            };
        case FETCH_USER_GOALPOINT_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_GOALPOINT]: action.payload
                }
            };

        //admin fetch user test goal point
        case FETCH_USER_TESTGOALPOINT:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [FETCH_USER_TESTGOALPOINT]: null
                }
            };
        case FETCH_USER_TESTGOALPOINT_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_TESTGOALPOINT]: null
                }
            };
        case FETCH_USER_TESTGOALPOINT_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [FETCH_USER_TESTGOALPOINT]: action.payload
                }
            };

        //admin checkout
        case ADMIN_CHECKOUT:
            return {
                ...state,
                processing: true,
                error: {
                    ...state.error,
                    [ADMIN_CHECKOUT]: null
                }
            };
        case ADMIN_CHECKOUT_SUCCESS:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_CHECKOUT]: null
                }
            };
        case ADMIN_CHECKOUT_FAILED:
            return {
                ...state,
                processing: false,
                error: {
                    ...state.error,
                    [ADMIN_CHECKOUT]: action.payload
                }
            };

        //clear error
        case CLEAR_ERROR:
            if (action.payload in state.error) {
                return {
                    ...state,
                    error: {
                        ...state.error,
                        [action.payload]: null
                    }
                };
            } else {
                return state;
            }

        //csrf token
        case CSRF_TOKEN:
            return {
                ...state,
                csrf: action.payload
            };
        default:
            return state;
    }
};

export default reducer;