import {
    FETCH_HISTORY, FETCH_HISTORY_FAILED, FETCH_HISTORY_SUCCESS,
    FETCH_GOALS, FETCH_GOALS_SUCCESS, FETCH_GOALS_FAILED,
    FETCH_GOALPOINT, FETCH_GOALPOINT_FAILED, FETCH_GOALPOINT_SUCCESS,
    FETCH_TESTGOALPOINT, FETCH_TESTGOALPOINT_FAILED, FETCH_TESTGOALPOINT_SUCCESS
} from "./actionNames";
import { getRecordHistories } from '../../services/record.service';
import { getGoals, getGoalPoints, getTestGoalPoints } from '../../services/goals.service';
//import { getRecordHistories } from '../../services/__fake__/record.server.fake';

const fetchHistory = () => ({
    type: FETCH_HISTORY
});

const fetchHistoryFailed = (error) => ({
    type: FETCH_HISTORY_FAILED,
    payload: error
});

const fetchHistorySuccess = (data) => ({
    type: FETCH_HISTORY_SUCCESS,
    payload: data
});

const processFetchHistory = (opts) => dispatch => {
    dispatch(fetchHistory());
    return getRecordHistories(opts).then(data => dispatch(fetchHistorySuccess(data)))
        .catch(error => dispatch(fetchHistoryFailed(error)));
};

const fetchGoals = () => ({
    type: FETCH_GOALS
});

const fetchGoalsFailed = (error) => ({
    type: FETCH_GOALS_FAILED,
    payload: error
});

const fetchGoalsSuccess = (data) => ({
    type: FETCH_GOALS_SUCCESS,
    payload: data
});

const processFetchGoals = () => dispatch => {
    dispatch(fetchGoals());
    return getGoals()
        .then(data => ({
            goals: data.goalPoint ?
                data.goalPoint :
                { data: [], pager: {} },
            testGoals: data.testGoalPoint ?
                data.testGoalPoint :
                { data: [], pager: {} }
        }))
        .then(data => dispatch(fetchGoalsSuccess(data)))
        .catch(error => dispatch(fetchGoalsFailed(error)));
};

const fetchGoalPoint = () => ({
    type: FETCH_GOALPOINT
});

const fetchGoalPointFailed = (error) => ({
    type: FETCH_GOALPOINT_FAILED,
    payload: error
});

const fetchGoalPointSuccess = (data) => ({
    type: FETCH_GOALPOINT_SUCCESS,
    payload: data
});

const processFetchGoalPoint = (opts) => dispatch => {
    dispatch(fetchGoalPoint());
    return getGoalPoints(opts)
        .then(data => dispatch(fetchGoalPointSuccess(data)))
        .catch(error => dispatch(fetchGoalPointFailed(error)));
};

const fetchTestGoalPoint = () => ({
    type: FETCH_TESTGOALPOINT
});

const fetchTestGoalPointFailed = (error) => ({
    type: FETCH_TESTGOALPOINT_FAILED,
    payload: error
});

const fetchTestGoalPointSuccess = (data) => ({
    type: FETCH_TESTGOALPOINT_SUCCESS,
    payload: data
});

const processFetchTestGoalPoint = (opts) => dispatch => {
    dispatch(fetchTestGoalPoint());
    return getTestGoalPoints(opts)
        .then(data => dispatch(fetchTestGoalPointSuccess(data)))
        .catch(error => dispatch(fetchTestGoalPointFailed(error)));
};

export { fetchHistory, fetchHistoryFailed, fetchHistorySuccess, processFetchHistory };
export { fetchGoals, fetchGoalsFailed, fetchGoalsSuccess, processFetchGoals };
export { fetchGoalPoint, fetchGoalPointFailed, fetchGoalPointSuccess, processFetchGoalPoint };
export { fetchTestGoalPoint, fetchTestGoalPointFailed, fetchTestGoalPointSuccess, processFetchTestGoalPoint };