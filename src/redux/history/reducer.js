import {
    FETCH_HISTORY, FETCH_HISTORY_FAILED, FETCH_HISTORY_SUCCESS,
    FETCH_GOALS, FETCH_GOALS_SUCCESS, FETCH_GOALS_FAILED,
    FETCH_TESTGOALPOINT, FETCH_TESTGOALPOINT_FAILED, FETCH_TESTGOALPOINT_SUCCESS,
    FETCH_GOALPOINT, FETCH_GOALPOINT_FAILED, FETCH_GOALPOINT_SUCCESS
} from "./actionNames";

const initialState = {
    histories: { data: [], pager: {} },
    goals: { data: [], pager: {} },
    testGoals: { data: [], pager: {} }
};

const historyReducer = (state = initialState, action) => {
    switch (action.type) {
        //history
        case FETCH_HISTORY:
        case FETCH_HISTORY_FAILED:
            return state;
        case FETCH_HISTORY_SUCCESS:
            return {
                ...state,
                histories: action.payload
            };

        //goals
        case FETCH_GOALS:
        case FETCH_GOALS_FAILED:
            return state;
        case FETCH_GOALS_SUCCESS:
            return {
                ...state,
                ...action.payload
            };

        //test-goal-point
        case FETCH_TESTGOALPOINT:
        case FETCH_TESTGOALPOINT_FAILED:
            return state;
        case FETCH_TESTGOALPOINT_SUCCESS:
            return {
                ...state,
                testGoals: action.payload
            };

        //goal-point
        case FETCH_GOALPOINT:
        case FETCH_GOALPOINT_FAILED:
            return state;
        case FETCH_GOALPOINT_SUCCESS:
            return {
                ...state,
                goals: action.payload
            };

        default:
            return state;
    }
};

export default historyReducer;