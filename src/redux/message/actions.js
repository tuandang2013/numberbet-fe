import { ADD_MESSAGE, REMOVE_MESSAGE, CLEAR_MESSAGE, CLEAR_TAGGED_MESSAGE, CLEAR_NOT_ERROR_MESSAGE } from "./actionNames";
import DOMPurify from 'dompurify';

const addMessage = (title, message, tag, isError) => ({
    type: ADD_MESSAGE,
    payload: {
        title,
        message: DOMPurify.sanitize(message),
        tag,
        error: !!isError,
        createdOn: new Date()
    }
});

const removeMessage = (item) => ({
    type: REMOVE_MESSAGE,
    payload: item
});

const clearMessage = () => ({
    type: CLEAR_MESSAGE
});

const clearTaggedMessage = (tag) => ({
    type: CLEAR_TAGGED_MESSAGE,
    payload: tag
});

const clearNotErrorMessage = () => ({
    type: CLEAR_NOT_ERROR_MESSAGE
});

export { addMessage, removeMessage, clearMessage, clearTaggedMessage, clearNotErrorMessage };