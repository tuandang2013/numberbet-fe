import { ADD_MESSAGE, REMOVE_MESSAGE, CLEAR_MESSAGE, CLEAR_TAGGED_MESSAGE, CLEAR_NOT_ERROR_MESSAGE } from './actionNames';

const initialState = {
    time: 3000,
    items: []
};

const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE:
            return {
                ...state,
                items: [...state.items, action.payload]
            };
        case REMOVE_MESSAGE: {
            const removeIndex = state.items.indexOf(action.payload);
            const items = Array.from(state.items);
            items.splice(removeIndex, 1);

            return {
                ...state,
                items
            };
        }
        case CLEAR_MESSAGE:
            return {
                ...state,
                items: []
            };
        case CLEAR_TAGGED_MESSAGE:
            return {
                ...state,
                items: Array.from(state.items).filter(item => item.tag !== action.payload)
            };
        case CLEAR_NOT_ERROR_MESSAGE:
            return {
                ...state,
                items: Array.from(state.items).filter(item => item.error)
            };
        default:
            return state;
    }
};

export default messageReducer;