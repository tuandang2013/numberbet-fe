import { combineReducers } from 'redux';
import todayReducer from './today/reducer';

const numberReducer = combineReducers({
    today: todayReducer
});

export default numberReducer;