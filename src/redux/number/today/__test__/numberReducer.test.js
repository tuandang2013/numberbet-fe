import numberReducer from '../reducer';
import { FETCH_TODAY, FETCH_TODAY_FAILED, FETCH_TODAY_SUCCESS } from '../actionNames';
import { fetchToday, fetchTodayFailed, fetchTodaySuccess } from '../actions';

describe('numberReducer', () => {
    test('should return current state if don\'t know action', () => {
        const action = { type: 'dontknowaction' };
        const currentState = {};

        const state = numberReducer(currentState, action);

        expect(state).toBe(currentState);
    });

    test('should error for fetch today failed', () => {
        const error = 'error';
        const action = fetchTodayFailed(error);
        const currentState = {};

        const state = numberReducer(currentState, action);

        expect(state).toEqual(Object.assign({}, state, { app: { processing: false, error } }));
    });

    test('should processing for fetch today', () => {
        const action = fetchToday();
        const currentState = {};

        const state = numberReducer(currentState, action);

        expect(state).toEqual(Object.assign({}, state, { app: { processing: true, error: null } }));
    });

    test('should return update state with payload for fetch today success', () => {
        const payload = { name: '' };
        const action = fetchTodaySuccess(payload);
        const currentState = {};

        const state = numberReducer(currentState, action);

        expect(state).toEqual(Object.assign({}, payload, { app: { processing: false, error: null } }));
    });

});