import { FETCH_TODAY, FETCH_TODAY_SUCCESS, FETCH_TODAY_FAILED } from './actionNames';
import { getLastest } from '../../../services/number';

const fetchToday = () => ({
    type: FETCH_TODAY
});

const fetchTodaySuccess = (data) => ({
    type: FETCH_TODAY_SUCCESS,
    payload: data
});

const fetchTodayFailed = (error) => ({
    type: FETCH_TODAY_FAILED,
    payload: error
});

function today() {
    return (dispatch) => {
        dispatch(fetchToday());
        return getLastest()
            .then(data => dispatch(fetchTodaySuccess(data)))
            .catch(error => dispatch(fetchTodayFailed(error)));
    };
};

export { fetchToday, fetchTodayFailed, fetchTodaySuccess };
export default today;