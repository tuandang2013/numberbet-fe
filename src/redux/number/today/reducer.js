import { FETCH_TODAY, FETCH_TODAY_SUCCESS, FETCH_TODAY_FAILED } from './actionNames';

const initialState = {
    name: null,
    url: null,
    date: null,
    numbers: {
        north: null,
        south: null
    },
    createdDate: null
};

const numberReducer = (state = initialState, action) => {
    switch (action.type) {
        //fetch today
        case FETCH_TODAY:
        case FETCH_TODAY_FAILED:
            return state;
        case FETCH_TODAY_SUCCESS:
            return {
                ...state,
                ...action.payload
            };

        default:
            return state;
    }
};

export default numberReducer;