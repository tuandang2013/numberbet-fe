const FETCH_INFO = '@app/play/info/FETCH_INFO';
const FETCH_INFO_SUCCESS = '@app/play/info/FETCH_INFO_SUCCESS';
const FETCH_INFO_FAILED = '@app/play/info/FETCH_INFO_FAILED';

export {
    FETCH_INFO, FETCH_INFO_SUCCESS, FETCH_INFO_FAILED
};