import { FETCH_INFO, FETCH_INFO_SUCCESS, FETCH_INFO_FAILED } from './actionNames';
import { getInfo } from '../../../services/number';

const fetchInfo = () => ({
    type: FETCH_INFO
});

const fetchInfoSuccess = (data) => ({
    type: FETCH_INFO_SUCCESS,
    payload: data
});

const fetchInfoFailed = (error) => ({
    type: FETCH_INFO_FAILED,
    payload: error
});

function info() {
    return (dispatch) => {
        dispatch(fetchInfo());
        return getInfo()
            .then(data => dispatch(fetchInfoSuccess(data)))
            .catch(error => dispatch(fetchInfoFailed(error)));
    };
};

export { fetchInfo, fetchInfoFailed, fetchInfoSuccess };
export default info;