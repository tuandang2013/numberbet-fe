import { FETCH_INFO, FETCH_INFO_SUCCESS, FETCH_INFO_FAILED } from './actionNames';
import fakeRecordInfoData from '../../__fake__/playInfo.fake';

const initialState = {
    init: false,
    areas: null,
    types: null
};

const playReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_INFO:
        case FETCH_INFO_FAILED:
            return state;
        case FETCH_INFO_SUCCESS:
            return {
                ...state,
                ...action.payload,
                init: true
            };
        default:
            return state;
    }
};

export default playReducer;