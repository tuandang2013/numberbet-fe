const CREATE_RECORD = '@app/play/record/CREATE_RECORD';
const CREATE_RECORD_SUCCESS = '@app/play/record/CREATE_RECORD_SUCCESS';
const CREATE_RECORD_FAILED = '@app/play/record/CREATE_RECORD_FAILED';

const FETCH_RECORDS = '@app/play/record/FETCH_RECORDS';
const FETCH_RECORDS_SUCCESS = '@app/play/record/FETCH_RECORDS_SUCCESS';
const FETCH_RECORDS_FAILED = '@app/play/record/FETCH_RECORDS_FAILED';

export { CREATE_RECORD, CREATE_RECORD_SUCCESS, CREATE_RECORD_FAILED, FETCH_RECORDS, FETCH_RECORDS_FAILED, FETCH_RECORDS_SUCCESS };