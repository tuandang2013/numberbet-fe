import { CREATE_RECORD, CREATE_RECORD_SUCCESS, CREATE_RECORD_FAILED, FETCH_RECORDS, FETCH_RECORDS_FAILED, FETCH_RECORDS_SUCCESS } from "./actionNames";
// import * as recordService from '../../../services/__fake__/record.server.fake';
import * as recordService from '../../../services/record.service';
import { pointInfo } from '../../user/actions';
import { recordSuccessMessage } from '../../../services/message.service';

const createRecord = () => ({
    type: CREATE_RECORD
});

const createRecordSuccess = (data) => ({
    type: CREATE_RECORD_SUCCESS,
    payload: data
});

const createRecordFailed = (error) => ({
    type: CREATE_RECORD_FAILED,
    payload: error
});

const processCreateRecord = (record, onComplete) => (dispatch) => {
    dispatch(createRecord());
    return recordService.createRecord(record)
        .then(data => {
            const { entry, pointInfo: { goalPoint, testGoalPoint } } = data;
            dispatch(pointInfo({ goalPoint, testGoalPoint }));
            dispatch(createRecordSuccess(entry));
        })
        .then(onComplete)
        .then(() => dispatch(recordSuccessMessage(record)))
        .catch(error => dispatch(createRecordFailed(error)));
};

const fetchRecords = () => ({
    type: FETCH_RECORDS
});

const fetchRecordsFailed = (error) => ({
    type: FETCH_RECORDS_FAILED,
    payload: error
});

const fetchRecordsSuccess = (data) => ({
    type: FETCH_RECORDS_SUCCESS,
    payload: data
});

const processFetchRecords = () => dispatch => {
    dispatch(fetchRecords());
    return recordService.getRecords().then(data => dispatch(fetchRecordsSuccess(data)))
        .catch(error => dispatch(fetchRecordsFailed(error)));
};


export { createRecord, createRecordSuccess, createRecordFailed, processCreateRecord };
export { fetchRecords, fetchRecordsFailed, fetchRecordsSuccess, processFetchRecords };