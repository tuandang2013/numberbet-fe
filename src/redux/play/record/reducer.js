import { CREATE_RECORD, CREATE_RECORD_SUCCESS, CREATE_RECORD_FAILED, FETCH_RECORDS, FETCH_RECORDS_SUCCESS, FETCH_RECORDS_FAILED } from './actionNames';

const initialState = {
    records: []
};

const recordReducer = (state = initialState, action) => {
    switch (action.type) {
        //create record
        case CREATE_RECORD:
        case CREATE_RECORD_FAILED:
        case CREATE_RECORD_SUCCESS:
            return state;

        //fetch records
        case FETCH_RECORDS:
        case FETCH_RECORDS_FAILED:
            return state;
        case FETCH_RECORDS_SUCCESS:
            return {
                ...state,
                records: action.payload
            };

        default:
            return state;
    }
};

export default recordReducer;