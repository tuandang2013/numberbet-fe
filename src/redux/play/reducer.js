import { combineReducers } from 'redux';
import infoReducer from './info/reducer';
import recordReducer from './record/reducer';

const playReducer = combineReducers({
    info: infoReducer,
    record: recordReducer
});

export default playReducer;