import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import numberReducer from './number/reducer';
import userReducer from './user/reducer';
import appReducer from './app/reducer';
import playReducer from './play/reducer';
import messageReducer from './message/reducer';
import historyReducer from './history/reducer';
import adminReducer from './admin/reducer';

const rootReducer = combineReducers({
    app: appReducer,
    numbers: numberReducer,
    user: userReducer,
    play: playReducer,
    message: messageReducer,
    history: historyReducer,
    admin: adminReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

export default store;