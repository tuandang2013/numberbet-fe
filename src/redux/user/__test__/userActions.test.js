import { signIn, login, loginFailed, loginSuccess, registerRequest, register, registerFailed, registerSuccess } from '../actions';
import { signIn as svcSignIn, register as svcRegister } from '../../../services/user';

jest.mock('../../../services/user');

describe('UserActions', () => {
    describe('signIn()', () => {
        test('should throw error if email is null', () => {
            expect(signIn).toThrowError();
        });

        test('should throw error if password is null', () => {
            const signInDetails = { email: 'email' };

            expect(() => signIn(signInDetails)).toThrowError();
        });

        test('should dispatch login action', () => {
            const signInDetails = { email: 'email', password: 'password' };
            const dispatch = jest.fn();
            svcSignIn.mockImplementation(() => Promise.resolve());

            signIn(signInDetails)(dispatch);

            expect(dispatch).toHaveBeenCalledWith(login());
        });

        test('should dispatch login failed for failed', (done) => {
            const signInDetails = { email: 'email', password: 'password' };
            const error = 'error';
            const dispatch = jest.fn();
            svcSignIn.mockRejectedValue(error);

            signIn(signInDetails)(dispatch).then(() => {
                expect(svcSignIn).toHaveBeenCalled();
                expect(dispatch.mock.calls[1][0]).toEqual(loginFailed(error));
                done();
            });
        });

        test('should dispatch login success for success', (done) => {
            const signInDetails = { email: 'email', password: 'password' };
            const signInResult = { data: { user: { email: 'email', token: 'token' } } };
            const dispatch = jest.fn();
            svcSignIn.mockResolvedValue(signInResult);

            signIn(signInDetails)(dispatch).then(() => {
                expect(dispatch.mock.calls[1][0]).toEqual(
                    loginSuccess({
                        signedIn: true,
                        info: {
                            email: signInResult.data.user.email
                        }, token: signInResult.data.user.token
                    }));
                done();
            });
        });
    });

    describe('registerRequest()', () => {
        test('should throw error if email is null', () => {
            expect(registerRequest).toThrowError();
        });

        test('should throw error if password is null', () => {
            const registerDetails = { email: 'email' };

            expect(() => registerRequest(registerDetails)).toThrowError();
        });

        test('should throw error if phone is null', () => {
            const registerDetails = { email: 'email', password: 'password' };

            expect(() => registerRequest(registerDetails)).toThrowError();
        });

        test('should dispatch register action', () => {
            const registerDetails = { email: 'email', password: 'password', phone: 'phone' };
            const dispatch = jest.fn();
            svcRegister.mockImplementation(() => Promise.resolve());

            registerRequest(registerDetails)(dispatch);

            expect(dispatch).toHaveBeenCalledWith(register());
        });

        test('should dispatch register failed for failed', (done) => {
            const registerDetails = { email: 'email', password: 'password', phone: 'phone' };
            const error = 'error';
            const dispatch = jest.fn();
            svcRegister.mockRejectedValue(error);

            registerRequest(registerDetails)(dispatch).then(() => {
                expect(svcRegister).toHaveBeenCalled();
                expect(dispatch.mock.calls[1][0]).toEqual(registerFailed(error));
                done();
            });
        });

        test('should dispatch register success for success', (done) => {
            const registerDetails = { email: 'email', password: 'password', phone: 'phone' };
            const registerResult = { data: { user: { email: 'email', token: 'token' } } };
            const dispatch = jest.fn();
            svcRegister.mockResolvedValue(registerResult);

            registerRequest(registerDetails)(dispatch).then(() => {
                expect(dispatch.mock.calls[1][0]).toEqual(
                    registerSuccess({
                        signedIn: true,
                        info: {
                            email: registerResult.data.user.email
                        }, token: registerResult.data.user.token
                    }));
                done();
            });
        });
    });
});