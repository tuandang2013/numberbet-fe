import userReducer from '../reducer';
import { login, loginSuccess, loginFailed, registerSuccess, registerFailed, register } from '../actions';

const processing = (container, process = false, error = null) => ({ app: { [container]: { processing: process, error: error } } })

describe('userReducer', () => {
    test('should return current state if don\'t know action', () => {
        const action = { type: 'dontknowaction' };
        const currentState = {};

        const state = userReducer(currentState, action);

        expect(state).toBe(currentState);
    });

    describe('login actions', () => {
        test('should login processing for login action', () => {
            const action = login();
            const currentState = {};

            const state = userReducer(currentState, action);

            expect(state).toEqual(Object.assign({}, currentState, processing('login', true)));
        });

        test('should update with payload for login success action', () => {
            const payload = { a: 'a', b: 'b' };
            const action = loginSuccess(payload);
            const currentState = {};

            const state = userReducer(currentState, action);

            expect(state).toEqual(Object.assign({}, currentState, payload, processing('login', false)));
        });

        test('should error for login failed action', () => {
            const action = loginFailed('error');
            const currentState = {};

            const state = userReducer(currentState, action);

            expect(state).toEqual(Object.assign({}, currentState, processing('login', false, 'error')));
        });

    });

    describe('register actions', () => {
        test('should processing for register action', () => {
            const action = register();
            const currentState = {};

            const state = userReducer(currentState, action);

            expect(state).toEqual(Object.assign({}, currentState, processing('register', true)));
        });

        test('should update with payload for register success action', () => {
            const payload = { a: 'a', b: 'b' };
            const action = registerSuccess(payload);
            const currentState = {};

            const state = userReducer(currentState, action);

            expect(state).toEqual(Object.assign({}, currentState, payload, processing('register', false)));
        });

        test('should error for register failed action', () => {
            const action = registerFailed('error');
            const currentState = {};

            const state = userReducer(currentState, action);

            expect(state).toEqual(Object.assign({}, currentState, processing('register', false, 'error')));
        });

    });
});