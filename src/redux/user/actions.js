import { LOGIN, LOGIN_FAILED, LOGIN_SUCCESS, REGISTER, REGISTER_FAILED, REGISTER_SUCCESS, USER_INFO, LOGOUT, POINT_INFO } from './actionNames';
import { signIn as svcSignIn, register as svcRegister } from '../../services/user.service';

const userInfo = (info) => ({
    type: USER_INFO,
    payload: info
});

const pointInfo = (pointInfo) => ({
    type: POINT_INFO,
    payload: pointInfo
});

const login = () => ({
    type: LOGIN
});

const loginSuccess = (data) => ({
    type: LOGIN_SUCCESS,
    payload: data
});

const loginFailed = (error) => ({
    type: LOGIN_FAILED,
    payload: error
});

const register = () => ({
    type: REGISTER
});

const registerSuccess = (data) => ({
    type: REGISTER_SUCCESS,
    payload: data
});

const registerFailed = (error) => ({
    type: REGISTER_FAILED,
    payload: error
});

const logout = () => ({
    type: LOGOUT
});

function signIn({ email, password, remember = false } = {}) {
    return (dispatch) => {
        dispatch(login());
        return svcSignIn({ email, password, remember })
            .then(response => {
                const payload = getUserInfoPayload(response);
                dispatch(loginSuccess(payload));
            })
            .catch(error => dispatch(loginFailed(error)));
    };
};

function registerRequest({ email, password, confirmPassword, phone } = {}) {
    return (dispatch) => {
        dispatch(register());
        return svcRegister({ email, password, confirmPassword, phone })
            .then(response => {
                const payload = getUserInfoPayload(response);
                dispatch(registerSuccess(payload));
            })
            .catch(error => dispatch(registerFailed(error)));
    };
};

function getUserInfoPayload(response) {
    const { data: { user: { email, token, goalPoint, testGoalPoint, claims } } } = response;
    const payload = { signedIn: true, info: { email, goalPoint, testGoalPoint, claims }, token };
    return payload;
}

export {
    userInfo, pointInfo,
    login, loginSuccess, loginFailed,
    signIn,
    register, registerFailed, registerSuccess,
    registerRequest,
    logout
};