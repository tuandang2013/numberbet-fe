import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILED, REGISTER_SUCCESS, REGISTER_FAILED, REGISTER, USER_INFO, LOGOUT, POINT_INFO } from './actionNames';
import fakeUserData from '../__fake__/user.fake';

const initialState = {
    signedIn: false,
    info: {
        email: null,
        goalPoint: 0,
        testGoalPoint: 0,
        claims: []
    },
    token: null
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_INFO:
            return {
                ...state,
                ...action.payload
            };
        case POINT_INFO:
            return {
                ...state,
                info: {
                    ...state.info,
                    ...action.payload
                }
            };
        case LOGIN:
            return {
                ...state,
                app: {
                    ...state.app,
                    login: { processing: true, error: null }
                }
            };
        case LOGIN_SUCCESS:
            return {
                ...state, ...action.payload,
                app: {
                    ...state.app,
                    login: { processing: false, error: null }
                }
            };
        case LOGIN_FAILED:
            return {
                ...state,
                app: {
                    ...state.app,
                    login: { processing: false, error: action.payload }
                }
            };
        case LOGOUT:
            return {
                ...state,
                signedIn: false,
                info: {
                    email: null
                },
                token: null
            };
        case REGISTER:
            return {
                ...state,
                app: {
                    ...state.app,
                    register: { processing: true, error: null }
                }
            };
        case REGISTER_SUCCESS:
            return {
                ...state, ...action.payload,
                app: {
                    ...state.app,
                    register: { processing: false, error: null }
                }
            };
        case REGISTER_FAILED:
            return {
                ...state,
                app: {
                    ...state.app,
                    register: { processing: false, error: action.payload }
                }
            };
        default:
            return state;
    }
};

export default userReducer;