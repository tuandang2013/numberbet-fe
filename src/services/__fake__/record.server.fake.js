import historyFakeData from '../../redux/__fake__/history.fake';

const fakeCreateRecordSuccessResult = (entry) => ({
    success: true,
    entry,
    pointInfo: {
        goalPoint: 999,
        testGoalPoint: 999
    }
});

const fakeCreateRecordFailedResult = ({
    success: false
});

export function createRecord(record) {
    console.log('Fake createRecord() run...', record);
    console.log('Fake createRecord() result', fakeCreateRecordSuccessResult);
    return Promise.resolve(fakeCreateRecordSuccessResult(record));
}

export function createRecordFailed(record) {
    console.log('Fake createRecordFailed() run...', record);
    console.log('Fake createRecordFailed() result', fakeCreateRecordFailedResult);
    return Promise.reject(fakeCreateRecordFailedResult);
}

export function getRecordHistories() {
    console.log('Fake getRecordHistories() run...');
    console.log('Fake getRecordHistories() result', historyFakeData);
    return Promise.resolve(historyFakeData);
}