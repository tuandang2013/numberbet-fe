import goalNames from '../goalNames';

describe('goalNames', () => {
    test('should have 9 constant values', () => {
        const expectedValues = ['G.8','G.7','G.6','G.5','G.4','G.3','G.2','G.1','G.ĐB'];

        expect(goalNames).toEqual(expectedValues);
    });
});