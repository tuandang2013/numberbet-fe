import { getError } from '../http';

describe('HttpService', () => {
    describe('getError()', () => {
        describe('have message', () => {
            test('should return message', () => {
                const responseError = { message: 'error' };

                const error = getError(responseError);

                expect(error).toEqual(responseError.message);
            });

        });

        describe('have response', () => {
            test('should return response', () => {
                const responseError = { response: 'error' };

                const error = getError(responseError);

                expect(error).toEqual(responseError.response);
            });

            describe('have error', () => {

                test('should return error message if have', () => {
                    const responseError = { response: { data: { error: { message: 'error' } } } };

                    const error = getError(responseError);

                    expect(error).toEqual(responseError.response.data.error.message);
                });

                test('should return error if error is string', () => {
                    const responseError = { response: { data: { error: 'error' } } };

                    const error = getError(responseError);

                    expect(error).toEqual(responseError.response.data.error);
                });

                test('should return array errors of object property values', () => {
                    const responseError = {
                        response: {
                            data: {
                                error: {
                                    email: 'email is required',
                                    password: 'password is required'
                                }
                            }
                        }
                    };

                    const error = getError(responseError);

                    expect(error).toEqual(['email is required', 'password is required']);
                });

            });
        });

        test('should return response error', () => {
            const responseError = 'Error';

            const error = getError(responseError);

            expect(error).toEqual(responseError);
        });
    });
});