jest.mock('../token', () => ({
    getToken: jest.fn()
}));
jest.mock('../user', () => ({
    userInfo: jest.fn()
}));
jest.mock('../http', () => ({
    get: jest.fn(),
    setCsrfToken: jest.fn()
}));

import { appInit, getCsrfToken, CSRF_TOKEN_URL } from '../initializer';
import { getToken } from '../token';
import { userInfo } from '../user';
import { userInfo as userInfoAction } from '../../redux/user/actions';
import http from '../http';
import { csrfToken } from '../../redux/app/actions';

describe('InitializerService', () => {
    describe('appInit()', () => {
        const store = { dispatch: jest.fn(), subscribe: jest.fn(), getState: jest.fn() };

        beforeEach(() => {
            getToken.mockReset();
            userInfo.mockReset();
            http.get.mockReset();
            store.getState.mockReset();
            store.dispatch.mockReset();
        });

        test('should throw error if store is null', () => {
            expect(appInit).toThrowError();
        });

        test('should not get user info if token not available', (done) => {
            getToken.mockReturnValue(null);
            http.get.mockResolvedValue({ data: {} });

            appInit(store).then(() => {
                expect(userInfo).not.toHaveBeenCalled();
                done();
            });
        });

        test('should get user info if token available', (done) => {
            const token = 'token';
            getToken.mockReturnValue(token);
            userInfo.mockResolvedValue({ data: {} });
            http.get.mockResolvedValue({ data: {} });

            appInit(store).then(() => {
                expect(userInfo).toHaveBeenCalled();
                done();
            });
        });

        test('should dispatch user info update action', (done) => {
            const token = 'token';
            getToken.mockReturnValue(token);
            const info = { email: 'email' };
            userInfo.mockResolvedValue({ data: info });
            const action = userInfoAction({ info, signedIn: true, token });
            http.get.mockResolvedValue({ data: {} });

            appInit(store).then(() => {
                expect(userInfo).toHaveBeenCalled();
                expect(store.dispatch.mock.calls[1][0]).toEqual(action);
                done();
            });
        });

        test('should request csrf token', (done) => {
            http.get.mockResolvedValue({ data: { token: 'token' } });

            appInit(store).then(() => {
                expect(http.get).toHaveBeenCalledWith(CSRF_TOKEN_URL);
                done();
            });
        });

        test('should dispatch csrf token action if get success', (done) => {
            const token = 'token';
            http.get.mockResolvedValue({ data: { token } });
            const csrfAction = csrfToken(token);

            appInit(store).then(() => {
                expect(http.get).toHaveBeenCalledWith(CSRF_TOKEN_URL);
                expect(store.dispatch).toHaveBeenCalledWith(csrfAction);
                done();
            });
        });
    });

    describe('getCsrfToken()', () => {
        test('should request token', (done) => {
            http.get.mockResolvedValue({ data: { token: 'token' } });

            getCsrfToken().then(() => {
                expect(http.get).toHaveBeenCalledWith(CSRF_TOKEN_URL);
                done();
            });
        });
    });
});