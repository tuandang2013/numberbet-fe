import { getLastest, LASTEST_URL } from '../number';
import http from '../http';
import { create } from 'react-test-renderer';

describe('NumberService', () => {
    describe('getLastest()', () => {
        it('should request api', (done) => {
            const data = { data: {} };
            jest.spyOn(http, 'get').mockResolvedValue(data);

            getLastest().then(result => {
                expect(result).toBe(data.data);
                expect(http.get).toHaveBeenCalledWith(LASTEST_URL);
                done();
            })
        });
    });
});