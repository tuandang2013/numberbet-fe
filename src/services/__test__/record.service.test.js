import { createRecord, RECORD_URL, getRecordDate, RECORD_DATE_URL } from '../record.service';
import http from '../http';

describe('RecordService', () => {
    describe('createRecord()', () => {
        it('should throw error if data is null', () => {
            expect(createRecord).toThrowError();
        });

        it('should request api', (done) => {
            const record = { record: {} };
            const data = { data: {} };
            jest.spyOn(http, 'post').mockResolvedValue(data);

            createRecord(record).then(result => {
                expect(result).toBe(data.data);
                expect(http.post).toHaveBeenCalledWith(RECORD_URL, record);
                done();
            });
        });
    });

    describe('getRecordDate()', () => {
        it('should request api', (done) => {
            const data = { data: 'date' };
            jest.spyOn(http, 'get').mockResolvedValue(data);

            getRecordDate().then(result => {
                expect(result).toBe(data.data);
                expect(http.get).toHaveBeenCalledWith(RECORD_DATE_URL);
                done();
            });
        });
    });
});