import { getToken, TOKEN_KEY, saveToken } from '../token';

const mockLocalStorage = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    removeItem: jest.fn()
};

describe('TokenService', () => {
    const originalLocalStorage = localStorage;

    beforeAll(() => {
        Object.defineProperty(window, 'localStorage', { value: mockLocalStorage });
    });
    afterAll(() => {
        Object.defineProperty(window, 'localStorage', { value: originalLocalStorage });
    });
    describe('getToken()', () => {
        afterEach(() => {
            localStorage.getItem.mockReset();
        });

        it('should return localStorage token', () => {
            const token = 'token';
            localStorage.getItem.mockReturnValue(token);

            const result = getToken();

            expect(result).toEqual(token);
        });
    });

    describe('saveToken()', () => {
        afterEach(() => {
            localStorage.setItem.mockReset();
        });

        it('should remove entry if token is null', () => {
            saveToken();

            expect(localStorage.setItem).not.toHaveBeenCalled();
            expect(localStorage.removeItem).toHaveBeenCalledWith(TOKEN_KEY);
        });

        it('should set item if token not null', () => {
            const token = 'token';

            saveToken(token);

            expect(localStorage.setItem).toHaveBeenCalledWith(TOKEN_KEY, token);
        });
    });
});