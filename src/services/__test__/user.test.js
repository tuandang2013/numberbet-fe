import { signIn, LOGIN_URL, register, REGISTER_URL, userInfo } from '../user';
import http from '../http';

describe('UserService', () => {
    describe('signIn()', () => {
        test('should throw error if email is null', () => {
            expect(signIn).toThrowError();
        });

        test('should throw error if password is null', () => {
            const loginInfo = { email: 'email' };
            expect(() => signIn(loginInfo)).toThrowError();
        });

        test('should error message if login failed', (done) => {
            const loginInfo = { email: 'invalid email', password: 'invalid password', remember: true };
            const errorMessage = 'error';
            jest.spyOn(http, 'post').mockRejectedValue(errorMessage);

            signIn(loginInfo).catch(error => {
                expect(error).toEqual(errorMessage);
                //expect(http, 'post').toHaveBeenCalledWith(LOGIN_URL, loginInfo);
                done();
            });
        });
    });

    describe('register()', () => {
        test('should throw error if email is null', () => {
            expect(register).toThrowError();
        });

        test('should throw error if password is null', () => {
            const registerInfo = { email: 'email' };
            expect(() => register(registerInfo)).toThrowError();
        });

        test('should throw error if phone is null', () => {
            const registerInfo = { email: 'email', password: 'password' };
            expect(() => register(registerInfo)).toThrowError();
        });

        test('should error message if register failed', (done) => {
            const registerInfo = { email: 'invalid email', password: 'invalid password', phone: 'phone' };
            const errorMessage = 'error';
            jest.spyOn(http, 'post').mockRejectedValue(errorMessage);

            register(registerInfo).catch(error => {
                expect(error).toEqual(errorMessage);
                //expect(http.post).toHaveBeenCalledWith(REGISTER_URL, registerInfo);
                done();
            });
        });
    });

    describe('userInfo()', () => {
        test('should request user info', (done) => {
            const info = { email: 'email' };
            jest.spyOn(http, 'get').mockResolvedValue(info);

            userInfo().then(result => {
                expect(result).toBe(info);
                done();
            });
        });
    });
});