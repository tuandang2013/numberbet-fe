import http from './http';

const CLAIM_URL = 'claims';

function getClaims() {
    return http.get(CLAIM_URL).then(({ data }) => data);
}

export {
    getClaims
};