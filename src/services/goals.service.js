import http from './http';
import moment from 'moment';
import {
    AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON,
    ADD_RECORD_ENTRY_GOAL_POINT_REASON,
    ADD_RECORD_ENTRY_TEST_GOAL_POINT_REASON,
    SUCCESS_GOAL_POINT_REASON,
    SUCCESS_TEST_GOAL_POINT_REASON,
    CHECKOUT_REASON
} from '../contants/goal-reason.const';

const GOAL_URL = 'goals';
const GOALPOINT_URL = 'goals/goal-point';
const TESTGOALPOINT_URL = 'goals/test-goal-point';

function getGoals() {
    return http.get(GOAL_URL).then(({ data }) => data);
}

function getUserGoals(id, { page, size } = {}) {
    return http.get(`${GOAL_URL}/user/${id}`, { params: { pageNumber: page, pageSize: size } }).then(({ data }) => data);
}

function getUserGoalPoint(id, { page, size } = {}) {
    return http.get(`${GOAL_URL}/user/${id}/goal-point`, { params: { pageNumber: page, pageSize: size } }).then(({ data }) => data);
}

function checkoutForUser(id, data) {
    return http.post(`${GOAL_URL}/user/${id}/checkout`, data).then(({ data }) => data);
}

function getUserTestGoalPoint(id, { page, size } = {}) {
    return http.get(`${GOAL_URL}/user/${id}/test-goal-point`, { params: { pageNumber: page, pageSize: size } }).then(({ data }) => data);
}

function getGoalPoints({ page = 1 } = {}) {
    return http.get(GOALPOINT_URL, { params: { pageNumber: page } }).then(({ data }) => data);
}

function getTestGoalPoints({ page = 1 } = {}) {
    return http.get(TESTGOALPOINT_URL, { params: { pageNumber: page } }).then(({ data }) => data);
}

const goalReasons = {
    [AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON]({ date }) {
        return `Điểm thưởng cộng ngày ${date}`;
    },
    [ADD_RECORD_ENTRY_GOAL_POINT_REASON]({ record }) {
        if (record == null)
            return '';
        return `Bạn đặt số ${record.number} với ${record.point} điểm ngày ${moment(record.date).format('DD/MM')}.`;
    },
    [ADD_RECORD_ENTRY_TEST_GOAL_POINT_REASON]({ record }) {
        if (record == null)
            return '';
        return `Bạn đặt số ${record.number} với ${record.point} điểm thưởng ngày ${moment(record.date).format('DD/MM')}.`;
    },
    [SUCCESS_GOAL_POINT_REASON]({ record }) {
        if (record == null)
            return '';
        return `Bạn ĐÃ TRÚNG con số ${record.number} (${record.point} điểm) đặt ngày ${moment(record.date).format('DD/MM')}.`;
    },
    [CHECKOUT_REASON]({ value, date }) {
        return `Bạn được hệ thống THANH TOÁN ${-value} điểm (${date}).`;
    },
    [SUCCESS_TEST_GOAL_POINT_REASON]({ record }) {
        if (record == null)
            return '';
        return `Bạn ĐÃ TRÚNG con số ${record.number} (${record.point} điểm thưởng) đặt ngày ${moment(record.date).format('DD/MM')}.`;
    }
};

export {
    goalReasons,
    getGoals, getUserGoals, getUserGoalPoint, getUserTestGoalPoint,
    getGoalPoints, getTestGoalPoints, checkoutForUser,
    GOAL_URL, GOALPOINT_URL, TESTGOALPOINT_URL
};