import axios from 'axios';
import { getToken } from './token';

const API_URL = 'http://localhost:3300/api';

let csrfToken = null;

const defaultInstance = axios.create({
    baseURL: API_URL,
    withCredentials: true
});

defaultInstance.interceptors.request.use(config => {
    const token = getToken();
    if (token != null)
        config.headers['Authorization'] = token;
    if (csrfToken != null)
        config.headers['X-CSRF-TOKEN'] = csrfToken;
    return config;
});

defaultInstance.interceptors.response.use(
    response => response,
    err => {
        const error = getError(err);
        return Promise.reject(error);
    }
);

function setCsrfToken(token) {
    csrfToken = token;
}

function getError(responseError) {
    let result;
    if (responseError.response) {
        const { data } = responseError.response;
        if (data) {
            let { error } = data;
            if (error) {
                if (typeof error == 'string') {
                    result = error;
                } else if (typeof error == 'object') {
                    if ('message' in error) {
                        result = error.message;
                    } else {
                        result = [];
                        for (const key of Object.getOwnPropertyNames(error)) {
                            result.push(error[key]);
                        }
                    }
                }
            } else if (data instanceof Array) {
                result = [];
                error = data.flat();
                for (const errorItem of error) {
                    for (const key of Object.getOwnPropertyNames(errorItem)) {
                        result.push(errorItem[key]);
                    }
                }
            } else {
                result = data;
            }
        } else {
            result = responseError.response;
        }
    } else if (responseError.message) {
        result = responseError.message;
    } else {
        result = responseError;
    }
    return result;
}

export { setCsrfToken, getError };
export default defaultInstance;