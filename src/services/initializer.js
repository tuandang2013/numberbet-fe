import { getToken, saveToken } from './token';
import { userInfo } from './user.service';
import { userInfo as userInfoAction } from '../redux/user/actions';
import { csrfToken, appStart, appStartFailed, appStartSuccess } from '../redux/app/actions';
import http, { setCsrfToken } from './http';

const CSRF_TOKEN_URL = 'common/csrf';

function appInit(store) {
    if (store == null)
        throw new Error('Store must be not null');

    store.dispatch(appStart());

    return Promise.all([getUserInfo(), getCsrfToken()]).then(([info, token]) => {
        if (info) {
            const action = userInfoAction(info);
            store.dispatch(action);
        }
        if (token) {
            const action = csrfToken(token);
            setCsrfToken(token);
            store.dispatch(action);
        }
        store.dispatch(appStartSuccess());
    }, (err) => store.dispatch(appStartFailed(`Application initialization is failed: ${err}`))).then(() => {
        store.subscribe(() => {
            const state = store.getState();
            const token = state.user.token;
            const originalToken = getToken();
            if (token !== originalToken) {
                saveToken(token);
            }
        });
    });
}

function getUserInfo() {
    const token = getToken();

    if (token != null) {
        return userInfo().then(response => ({
            token,
            signedIn: true,
            info: {
                email: response.data.email,
                goalPoint: response.data.goalPoint,
                testGoalPoint: response.data.testGoalPoint,
                claims: response.data.claims
            }
        }));
    }
    return Promise.resolve();
}

function getCsrfToken() {
    return http.get(CSRF_TOKEN_URL).then(response => response.data.token);
}

export { appInit, getUserInfo, getCsrfToken, CSRF_TOKEN_URL };