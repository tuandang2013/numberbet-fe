import { TEST_GOAL_POINT } from '../contants/point.const';
import { addMessage } from '../redux/message/actions';
import moment from 'moment';
import { PLAY_MESSAGE_TAG, GLOBAL_MESSAGE_TAG } from '../contants/message-tag.const';

function recordSuccessMessage({ number, point, pointType, date }) {
    return addMessage(
        'Đặt số thành công',
        `<span>Bạn đã đặt 
            số <strong>${number}</strong> 
            với <strong>${point} điểm ${pointType === TEST_GOAL_POINT ? 'thưởng' : ''}</strong> 
            ngày <strong>${moment(date).format('DD/MM')}</strong>.
        <br/>Vui lòng quay lại vào ngày mai để biết kết quả...</span>`,
        GLOBAL_MESSAGE_TAG);
}

function notEnoughPointMessage(type) {
    return addMessage(
        'Bạn không đủ điểm chơi',
        `Bạn không đủ điểm để chơi '${type}'. Vui lòng chọn cách chơi khác hoặc liên hệ với chúng tôi để tiếp tục chơi`,
        PLAY_MESSAGE_TAG,
        true
    );
}

export { recordSuccessMessage, notEnoughPointMessage };