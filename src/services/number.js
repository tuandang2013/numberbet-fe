import http from './http';

const LASTEST_URL = 'numbers/lastest';
const INFO_URL = 'records/info/today';

const getLastest = () => {
    return http.get(LASTEST_URL).then(({ data }) => data);
};

const getInfo = () => {
    return http.get(INFO_URL).then(({ data }) => data);
};

export { getLastest, getInfo, LASTEST_URL, INFO_URL };