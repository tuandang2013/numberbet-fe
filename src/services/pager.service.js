function getPages(totalPages, currentPage, limit) {
    const margin = limit % 2 === 0 ? limit / 2 : (limit - 1) / 2;
    const total = Math.min(limit, totalPages);
    let pages = [];
    if (currentPage >= margin) {
        for (let i = margin; i > 0; i--) {
            if (currentPage - i > 0) {
                pages.push(currentPage - i);
            }
        }
    }
    if (!pages.includes(currentPage)) {
        pages.push(currentPage);
    }

    const length = pages.length;
    for (let i = 1; i <= total - length; i++) {
        if (currentPage + i <= totalPages) {
            pages.push(currentPage + i);
        }
    }

    if (pages.length < limit) {
        for (let i = 1, length = pages.length; i <= limit - length; i++) {
            if (pages[0] - i > 0) {
                pages.unshift(pages[0] - 1);
            }
        }
    }

    return [pages, margin];
}

export { getPages };