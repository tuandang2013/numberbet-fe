import http from './http';

const RECORD_URL = 'records';
const RECORD_DATE_URL = 'records/date'
const RECORD_HISTORY_URL = 'records/history';

function createRecord(record) {
    if (record == null)
        throw new Error('Record is required');
    return http.post(RECORD_URL, record).then(({ data }) => data);
}

function getRecordDate() {
    return http.get(RECORD_DATE_URL).then(({ data }) => data);
}

function getRecordHistories({ page = 1 } = {}) {
    return http.get(RECORD_HISTORY_URL, { params: { pageNumber: page } }).then(({ data }) => data);
}

function getRecords() {
    return http.get(RECORD_URL).then(({ data }) => data);
}

function getUserRecords(id, { page = 1, size = 12 } = {}) {
    return http.get(`${RECORD_URL}/user/${id}`, { params: { pageNumber: page, pageSize: size } })
        .then(({ data }) => data);
}

export {
    createRecord, RECORD_URL,
    getRecordDate, RECORD_DATE_URL,
    getRecordHistories, RECORD_HISTORY_URL,
    getRecords,
    getUserRecords
};