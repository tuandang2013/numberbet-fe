const TOKEN_KEY = '@app/user/token';

function getToken() {
    return localStorage.getItem(TOKEN_KEY);
}

function saveToken(token) {
    if (token != null)
        localStorage.setItem(TOKEN_KEY, token);
    else
        localStorage.removeItem(TOKEN_KEY);
}

export { getToken, saveToken, TOKEN_KEY };