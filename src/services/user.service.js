import http from './http';

const USER_URL = 'users';
const LOGIN_URL = 'users/login';
const REGISTER_URL = 'users/register';
const INFO_URL = 'users/info';

function userInfo() {
    return http.get(INFO_URL);
}

function signIn({ email, password, remember = false } = {}) {
    return http.post(LOGIN_URL, { email, password, remember });
}

function register({ email, password, confirmPassword, phone } = {}) {
    return http.post(REGISTER_URL, { email, password, confirmPassword, phone });
}

function getUsers({ page = 1 } = {}) {
    return http.get(USER_URL, { params: { pageNumber: page } }).then(({ data }) => data);
}

function deleteUser(id) {
    return http.delete(`${USER_URL}/${id}`);
}

function getUserDetails(id) {
    return http.get(`${USER_URL}/${id}`).then(({ data }) => data);
}

function updateUser(id, details) {
    return http.put(USER_URL, { id, ...details }).then(({ data }) => data);
}

function changeUserPass(id, changePassData) {
    return http.post(`${USER_URL}/${id}/change-password`, changePassData);
}

export {
    signIn, LOGIN_URL,
    register, REGISTER_URL,
    userInfo, INFO_URL,
    getUsers,
    deleteUser,
    getUserDetails,
    updateUser,
    changeUserPass
};